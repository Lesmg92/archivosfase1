#include "Analizador.h"
#include "GestorDiscos.h"
/*------------------------------------------------------------------------------------*/
/*---------------------------METODOS Y FUNCIONES AUXILIARES---------------------------*/
/*------------------------------------------------------------------------------------*/

bool startsWith(const char *pre, const char *str){
    int n = strncasecmp(pre, str, strlen(pre)) == 0;
    return n;
}

int buscarEspacios(char *token,char s){
        if (!token || s=='\0')
        return 0;
    for (;*token; token++)
        if (*token == s)
            return 1;
    return 0;
}

char *quitarEspacios(char *tmp,const char *cadena) {

    while((tmp=strchr(cadena,32))!=NULL || (tmp=strchr(cadena,10))!=NULL)
        sprintf(tmp,"%s",tmp+1);
    return tmp;
}

void quitarUltimoEspacio(char str[256]) {
    int ultimo = strlen(str)-1;
    char aux = str[ultimo];
    if(aux == 32 || aux == '\n'){
        str++[ultimo] = '\0';
        }
   }

void analyzeFile(char path[256]){
    FILE *fptr;
    fptr = fopen(path, "r");
    char linea[256];

    while(!feof(fptr)){
        fgets(linea,256,fptr);
        printf("\nLINEA: --- %s\n", linea);
        orden(linea);
    }

    fclose(fptr); 
}


int verificarArchivoE(char path[300]) {
    if(access(path, F_OK ) != -1 ) {
     return 1;
    }
    return 0;
}

void crearDirectorio(char path[300]) {
    char *dir = strdup(path);
    size_t tamDir = strlen(dir);
    size_t i;
    char pathActual[tamDir + 1];
    memset(pathActual, 0, sizeof pathActual);
    pathActual[0] = '/'; 
    int dirExistente = 1;
    for(i = 1; i < tamDir; i++){
        if(path[i] == '/'){
            if(dirExistente == 1) {
                int verificarPath = verificarArchivoE(pathActual);
                if(verificarPath == 0){
                    int status = mkdir(pathActual,0777);
                    if(status == -1) {
                        printf("Error. No se pudo crear el directorio.\n");
                        break;
                    }
                    dirExistente = 0; 
                }
            }
            else 
            {
                int status = mkdir(pathActual,0777);
                if(status == -1) {
                    printf("Error. No se pudo crear el directorio.\n");
                    break;
                }
            }
        }
        pathActual[i] = path[i];
        pathActual[i+1] = '\0';
    }


}


/*------------------------------------------------------------------------------------*/
/*---------------------------VERIFICAR PARAMETROS CORRECTOS---------------------------*/
/*------------------------------------------------------------------------------------*/

bool nameDskOk(char s[]){
     int c = 0;
   while (s[c] != '\0') {
      if ((s[c] >= 'a' && s[c] <= 'z') || (s[c] >= 'A' && s[c] <= 'Z') || (s[c] >= '0' && s[c] <= '9') || s[c] == '_' || s[c] == '.'|| s[c]==32) {
      } else {
      printf("\t*** Error nombre mal Disco\n");  
      return false;
      }
      c++;
   }
   if(strstr(s, ".dsk") == NULL) {
      return false;
    }
    return true;
}

bool nameOk(char s[]){
     int c = 0;
   while (s[c] != '\0') {
      if ((s[c] >= 'a' && s[c] <= 'z') || (s[c] >= 'A' && s[c] <= 'Z')  || (s[c] >= '0' && s[c] <= '9') || s[c] == '_') {
      } else {
      return false;
      }
      c++;
   }
    return true;
}

bool pathOk(char *str){
    if(strchr(str, 32) != NULL){
      if(strchr(str, 34) != NULL){
          if(str[0] == 34 || str[strlen(str)-1] == 34){
              return true;
            }else{
              return false; //comillas fuera de lugar
            }
      } else {
        return false; // contiene espacios y no comillas
      }
    }
   return true;
}

bool unitOk(char car){
    if(car == 'b' || car == 'k' || car == 'm'){
        return true;
    }
    return false;
}

bool typeOk(char car){
    if(car == 'p' || car == 'e' || car == 'l'){
        return true;
    }
    return false;

}

bool fitOk(const char *str){
       if(strcasecmp(str, "bf") == 0 || strcasecmp(str, "ff") == 0 || strcasecmp(str, "wf") == 0 ) {
      return true;
    }
    return false;
}

bool allocationtOk(const char *str){
       if(strcasecmp(str, "c") == 0 || strcasecmp(str, "e") == 0 || strcasecmp(str, "ix") == 0 ) {
      return true;
    }
    return false;
}

bool deleteOk(const char *str){
       if(strcasecmp(str, "fast") == 0 || strcasecmp(str, "full") == 0 ) {
      return true;
    }
    return false;
}

/*------------------------------------------------------------------------------------*/
/*-------------------------------ACCIONES DE CADA COMANDO-----------------------------*/
/*------------------------------------------------------------------------------------*/

void goToComment(char str[256]){
    printf("Comentario: %s \n",str);
}

void goToMkdisk(char str[256]){
        int mk_size = 0;
        char mk_name[256];
        char mk_path[256];
        char mk_unit = 'm';

        const char s[2] = "%&";
        char *token;
        char *end_token;

        bool bmk_size = false;
        bool bmk_path = false;
        bool bmk_name = false;
        token = strtok_r(str, s,&end_token);
        token = strtok_r(NULL,s,&end_token);
             while( token != NULL ){
                      const char aptr[2] = "->";
                      char *end_atributo;
                      char * atributo = strtok_r(token,aptr,&end_atributo);
                      
                        char aux[256];
                        strcpy(aux,token);
                      if(startsWith("size",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          mk_size = atoi(atributo);
                          int divMod = mk_size % 8;
                          if(divMod == 0 && mk_size>0){
                              bmk_size = true;
                              }else{
                              printf("**Error el numero en mkdisk no es multiplo de 8 o es negativo\n");
                                  }
                      }else if(startsWith("path",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarUltimoEspacio(atributo);
                      if(pathOk(atributo)){
                            bmk_path = true;
                            strcpy(mk_path, atributo);
                          }
                      }else if(startsWith("name",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          strcpy(mk_name, atributo);
                          quitarUltimoEspacio(mk_name);
                          if(nameDskOk(mk_name)){
                          bmk_name = true;
                          }
                      }else if(startsWith("unit",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          mk_unit = atributo[0];
                          if(!unitOk(mk_unit)){
                            // error en unit
                            mk_unit = 'k';
                          }
                      }else {
                       bmk_name = false;
                       }
                      token = strtok_r(NULL,s,&end_token);
                   } 
                   if(bmk_name && bmk_path && bmk_size){
                        printf("\nLos parametros MKDISK son: \nsize:%d \npath:%s \nname:[%s] \nunit:%c \n", mk_size, mk_path, mk_name, mk_unit);                        
                        if(startsWith("\"",mk_path))
                        {
                            char *auxp;
                            auxp = strtok(mk_path,"\"");
                            strcpy(mk_path,auxp);
                        }
                        crearDirectorio(mk_path);
                        strcat(mk_path,"/"); 
                        strcat(mk_path,mk_name);
                       crearDisco(mk_path,mk_size,mk_unit);
                   }else{
                        printf("\nPARAMETROS MAL MKDISK\n");
                   }
}

void goToRmdisk(char str[256]){
    char rm_path[256];
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);
    bool itsOk = false;

    while( token != NULL ){
         const char aptr[2] = "->";
         char *end_atributo;
         char *atributo; 
         atributo = strtok_r(token,aptr,&end_atributo);
         
        char aux[256];
        strcpy(aux,token);
        if(startsWith("path", aux)){
             atributo = strtok_r(NULL,aptr,&end_atributo);
             char auxat[256];
             strcpy(auxat,atributo);
             quitarUltimoEspacio(auxat);
            if(pathOk(auxat)){
                strcpy(rm_path,auxat);
                itsOk = true;
            } else {
              printf("\t*** Error path rmdisk\n");
            }
        }else{
                        printf("\nPARAMETROS MAL RMDISK\n");
                        itsOk = false;
                   }
        token = strtok_r(NULL,s,&end_token);
    }
    
    if(itsOk){
            eliminarDisco(rm_path);
        }
    
}

void goToFdisk(char str[256]){
    int f_size = 0;
    char f_unit = 'k';
    char f_path[256];
    char f_type = 'p';
    char f_fit = 'w';
    char f_allocation = 'i';
    char f_delete[256]; // verificar junto a name y path
    char f_name[256];
    int f_add = 0;  // valor unit

    bool bf_size = false;
    bool bf_path = false;
    bool bf_name = false;
    bool bf_useDelete = false;
    bool bf_useAdd = false;

        const char s[2] = "%&";
        char *token;
        char *end_token;
        token = strtok_r(str, s,&end_token);
        token = strtok_r(NULL,s,&end_token);
             while( token != NULL ){
                      const char aptr[2] = "->";
                      char *end_atributo;
                      char * atributo = strtok_r(token,aptr,&end_atributo);
                      
                        char aux[256];
                        strcpy(aux,token);
                      if(startsWith("size",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_size = atoi(atributo);
                          int divMod = f_size % 8;
                          if(divMod == 0 && f_size>0){
                              bf_size = true;
                              }else{
                              printf("**Error el numero en fdisk no es multiplo de 8 o es negativo\n");
                                  }
                      }else if(startsWith("unit",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_unit = atributo[0];
                          if(!unitOk(f_unit)){
                            // error en unit
                            f_unit = 'k';
                          }
                      }else if(startsWith("path",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarUltimoEspacio(atributo);
                          if(pathOk(atributo)){
                            strcpy(f_path, atributo);
                            bf_path = true;
                          }
                      }else if(startsWith("type",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_type = atributo[0];
                          if(!typeOk(f_type)){
                            // error en type
                            f_type = 'p';
                          }
                      }else if(startsWith("fit",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_fit = atributo[0];
                          if(!fitOk(f_fit)){
                            f_fit = 'w';
                          }
                      }else if(startsWith("allocation",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarEspacios(f_allocation,atributo);
                          if(!allocationtOk(f_allocation)){
                            // error en fit
                            f_allocation = 'i';
                          }
                      }else if(startsWith("delete",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarEspacios(f_delete,atributo);
                          if(!deleteOk(f_delete)){
                            // error en delete
                          }
                          bf_useDelete = true;
                      }else if(startsWith("name",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarEspacios(f_name,atributo);
                          strcpy(f_name,atributo);
                          bf_name = true;
                      }else if(startsWith("add",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_add = atoi(atributo);
                          bf_useAdd = true;
                      } else {
                          bf_name = false;
                          bf_path = false;
                          }
                      token = strtok_r(NULL,s,&end_token);
             }

            //// ------> verificar parametros completos para el uso de fdisk
             if(bf_useDelete){
              if(bf_name && bf_path){
                        printf("\nLos parametros FDISK(DELETE) son: \nname:%s \npath:%s \n", f_name, f_path);
                        eliminarParticion(f_path,f_name);
                   }else{
                        printf("\nPARAMETROS MAL FDISK");
                   }

             } else if(bf_useAdd){
              if(bf_name){
                                if(f_unit=='m' || f_unit=='M') 
                                    f_add=f_add*mb;
                                else if(f_unit=='k' || f_unit=='K')
                                    f_add=f_add*kb;
                        printf("\nLos parametros FDISK(ADD) son: \nname:%s \nunit:%c \n", f_name, f_unit);
                        modificarParticion(f_path,f_name,f_add);
                   }else{
                        printf("\nPARAMETROS MAL FDISK");
                   }
             } else {
              if(bf_name && bf_path && bf_size){
                        printf("\nLos parametros FDISK son: \nsize:%d \npath:%s \nname:%s \nunit:%c \n", f_size, f_path, f_name, f_unit);
                         
                                if(f_unit=='m' || f_unit=='M') 
                                    f_size=f_size*mb;
                                else if(f_unit=='k' || f_unit=='K')
                                    f_size=f_size*kb;
                        if(startsWith("\"",f_path))
                        {
                            char *auxp;
                            auxp = strtok(f_path,"\"");
                            strcpy(f_path,auxp);
                        }            
                        crearParticion(f_path, f_name, f_type,f_fit,f_size);
                   }else{
                        printf("\nPARAMETROS MAL FDISK");
                   }
             }
}

void goToMount(char str[256]){
        char m_path[256];
        char m_name[50];

        const char s[2] = "%&";
        char *token;
        char *end_token;

        bool bm_name = false;
        bool bm_path = false;

        token = strtok_r(str, s,&end_token);
        if(token != NULL)
            {
                token = strtok_r(NULL,s,&end_token);
            }
            else
            {
                printf("\n");    
            }
        
             while( token != NULL ){
                      const char aptr[2] = "->";
                      char *end_atributo;
                      char * atributo = strtok_r(token,aptr,&end_atributo);
                      
                        char aux[256];
                        strcpy(aux,token);
                      if(startsWith("name",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          strcpy(m_name,atributo);
                          quitarUltimoEspacio(m_name);
                          if(nameOk(m_name)){
                          bm_name = true;
                          }
                      }else if(startsWith("path",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          strcpy(m_path,atributo);
                          quitarUltimoEspacio(m_path);
                          if(pathOk(m_path)){
                            bm_path = true;
                          }
                      }else {
                          bm_name = false;
                          bm_path = false;
                          }
                       token = strtok_r(NULL,s,&end_token);
                   }
                   if(bm_name && bm_path){
                        printf("\nLos parametros MOUNT son: \npath:%s \nname:%s \n", m_path, m_name);
                        mountPart(m_path, m_name);
                   }else{
                        printListMountedP();
                        printf("\nMount sin parmetros imprime valores\n");
                   }
}

void goToUnmount(char str[256]){
    
    char *umo_id;
    
    const char s[2] = "&%";
    char *token;
    char *end_token;
    
    bool itsOk = false;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);
    
     while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        
        char aux[256];
        strcpy(aux,token);
         if(startsWith("id", aux)){
             atributo = strtok_r(NULL,aptr,&end_atributo);
             quitarUltimoEspacio(atributo);
            if(pathOk(atributo)){
                strcpy(umo_id, atributo);
                itsOk = true;
            } else {
                itsOk = false;
                printf("Unmount error en el path\n");
            }
        }else {
                itsOk = false;
               }
        token = strtok_r(NULL,s,&end_token);
     }
     
     if(itsOk){
            Uumount_Metodo(umo_id);
         }
}

void goToDf(char str[256]){
    const char s[2] = "%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

        char aux[256];
        strcpy(aux,token);

    if(startsWith("k", aux)){
    
    }else if(startsWith("m", aux)){
    
    }else if(startsWith("h", aux)){
        
    }else if(startsWith("i", aux)){
        
    }else{
    // no parametro a consultar    
    }
}

void goToDu(char str[256]){
    const char s[2] = "%&";
    char *token;
    char *end_token;
    char du_path[256];
    int du_n = 1;
    
    bool bdu_path = false;
    bool bdu_h = false;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

         while( token != NULL ){
             const char aptr[2] = "->";
             char *end_atributo;
             char * atributo = strtok_r(token,aptr,&end_atributo);
             
            char aux[256];
            strcpy(aux,token);
            if(startsWith("n", aux)){
                 atributo = strtok_r(NULL,aptr,&end_atributo);
                 du_n = atoi(atributo);
            }else if(startsWith("h", aux)){
                bdu_h = true;
            }else if(startsWith("path", aux)){
                 atributo = strtok_r(NULL,aptr,&end_atributo);
                 quitarUltimoEspacio(atributo);
                if(pathOk(atributo)){
                    strcpy(du_path, atributo);
                    bdu_path = true;
                    } else {
                  // error en el atributo
                    }
            }else{
            // parametro de más  
            }
            token = strtok_r(NULL,s,&end_token);
    }
    if(bdu_h && bdu_path){
       printf("\nLos parametros DU son: \npath:%s", bdu_path);
    }else{
       printf("\nPARAMETROS MAL DISK USED");
    }
}

void goToMkfs(char str[256]){
    
    char mkfs_type[6];
    char mkfs_id[5];
    char *mkfs_fs;
    int mkfs_add;
    char mkfs_unit;
    
    bool bmkfs_id = false;
    bool bmkfs_add = false;
    
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

    
    
    while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        
        char aux[256];
        strcpy(aux,token);
        if(startsWith("id", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            strcpy(mkfs_id, atributo);
            quitarUltimoEspacio(mkfs_id);
            bmkfs_id = true;
        }else if(startsWith("type", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            quitarEspacios(mkfs_type,atributo);
            if(!deleteOk(mkfs_type)){
               // error en delete
            }
        }else if(startsWith("add", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            mkfs_add = atoi(atributo);
            bmkfs_add = true;
        }else if(startsWith("unit", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            mkfs_unit = atributo[0];
            if(!unitOk(mkfs_unit)){
               // error en unit
               mkfs_unit = 'k';
            }
        }else if(startsWith("fs", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            strcpy(mkfs_fs,atributo);
        }
        token = strtok_r(NULL,s,&end_token);
    }               
         if(bmkfs_id)
            {
                if(bmkfs_add)
                    {
                        modificarFormatoPart(mkfs_id, mkfs_add, mkfs_unit);       
                    }
                    else
                    {
                        formatoParticion(mkfs_id, mkfs_type, mkfs_fs);   
                    }
            } 
            else 
            {
                 printf("En mkfs falta id o es incorrecto\n");
            }
}

void goToMkfile(char str[256]){

    char *mkf_id;
    char *mkf_name;
    char *mkf_contenido;
    
    bool bmkf_id = false;
    bool bmkf_name = false;
    bool bmkf_contenido = false;
    
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

    
    while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        
        char aux[256];
        strcpy(aux,token);
        if(startsWith("id", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            strcpy(mkf_id, atributo);
            bmkf_id = true;
        }else if(startsWith("name", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            quitarEspacios(mkf_name,atributo);
            bmkf_name = true;
        }else if(startsWith("contenido", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            strcpy(mkf_contenido, atributo);
            bmkf_contenido = true;
        }
        token = strtok_r(NULL,s,&end_token);
    }
    
    if(bmkf_id && bmkf_name && bmkf_contenido)
        {
           guardarArchivo(mkf_id, mkf_name, mkf_contenido); 
        }
        else
        {
            printf("En mkfile faltan parámetros\n");
        }
}

void goToRep(char str[256]){    
    
    char rep_name[50];
    char rep_path[256];
    char rep_id[50];
    
    const char s[2] = "&";
    char *token;
    char *end_token;
    
    bool brep_id = false;
    bool brep_name = false;
    bool brep_path = false;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

    
    while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char *atributo = strtok_r(token,aptr,&end_atributo);
        
        char aux[256];
        strcpy(aux,token);
        if(startsWith("id", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            strcpy(rep_id, atributo);
            quitarUltimoEspacio(rep_id);
            brep_id = true;
        }else if(startsWith("name", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            quitarEspacios(rep_name,atributo);
            strcpy(rep_name,atributo);
            quitarUltimoEspacio(rep_name);
            brep_name = true;
        }else if(startsWith("path", aux)){
             atributo = strtok_r(NULL,aptr,&end_atributo);
             quitarUltimoEspacio(atributo);
            if(pathOk(atributo)){
                strcpy(rep_path, atributo);
                brep_path = true;
            } else {
              // error en el atributo path
            }
     
        }
        token = strtok_r(NULL,s,&end_token);
    }
    
    if(brep_id && brep_name && brep_path)
        {
            if(strcasecmp(rep_name, "mbr")==0)
            {
                repMBR(rep_id);
            }
            else if(strcasecmp(rep_name, "disk")==0)
            {
                repDSK(rep_id, rep_path);
            }
            else if(strcasecmp(rep_name, "block")==0)
            {
                repBloques(rep_id,rep_path); 
            }
            else if(strcasecmp(rep_name, "bm_block")==0)
            {
                repBitMap(rep_id,rep_path);
            }
        }
        else
            {
            printf("Faltan parametros en Rep\n");
            }

}

void goToExec(char str[256]){
    
    char exe_path[256];
    
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);
    
     while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        char aux[256];
        strcpy(aux,token);
         if(startsWith("path", aux)){
             atributo = strtok_r(NULL,aptr,&end_atributo);
             quitarUltimoEspacio(atributo);
            if(pathOk(atributo)){
                strcpy(exe_path, atributo);
            } else {
              // error en el atributo path
            }
        }
        token = strtok_r(NULL,s,&end_token);
     }
     
     analyzeFile(exe_path); // exec &path->/home/leslie/Escritorio/archivo.txt

}

/*------------------------------------------------------------------------------------*/
/*-------------------------REDIRIGIR ORDEN RESPECTIVA---------------------------------*/
/*------------------------------------------------------------------------------------*/

void orden(char str[256]){
    char aux[256];
    strcpy(aux, str);
    if(startsWith("#", aux)){
    goToComment(str);
    }else if(startsWith("mkdisk", aux)){
    goToMkdisk(str);
    }else if(startsWith("rmdisk",aux)){
    goToRmdisk(str);
    }else if(startsWith("fdisk",aux)){
    goToFdisk(str);
    }else if(startsWith("mount",aux)){
    goToMount(str);
    }else if(startsWith("unmount",aux)){
    goToUnmount(str);
    }else if(startsWith("df",aux)){
    goToDf(str);
    }else if(startsWith("du",aux)){
    goToDu(str);
    }else if(startsWith("mkfs",aux)){
    goToMkfs(str);
    }else if(startsWith("mkfile",aux)){
    goToMkfile(str);
    }else if(startsWith("rep",aux)){
    goToRep(str);
    }else if(startsWith("exec",aux)){
    goToExec(str);
    }
}


