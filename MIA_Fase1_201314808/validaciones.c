#include "validaciones.h"
//metodo para insertar
void VerificarDiscoPart(VerFer x){
    enlace1D= (struct VerFer*)malloc(1*sizeof(struct VerD));
    enlace1D->anteriorD1 = NULL;
    enlace1D->siguienteD1 = NULL;
    enlace1D->valor=x;

    if (inicioD == NULL){
     inicioD = enlace1D;
     enlace1D=inicioD;
 } else {
     enlace1D->siguienteD1= inicioD;
     inicioD->anteriorD1= enlace1D;
     inicioD= enlace1D;
 }
}


//metodo para imprimir

void imprimirNodoDVerificarDiscoPart(){
    enlace2D=inicioD;
    if(enlace2D == NULL){
       printf("No hay particiones montadas\n");
       return -1;
    }

    while(enlace2D != NULL){
        printf("Nombre p-n:[%s]-[%c]\n",enlace2D->valor.path,enlace2D->valor.name);
        enlace2D=enlace2D->siguienteD1;
    }


}


int tamanoLista(){
    int recorrido=0;
    enlace2D=inicioD;
    if(enlace2D == NULL){
       return -1;
    }

    while(enlace2D != NULL){
        recorrido++;
        enlace2D=enlace2D->siguienteD1;
    }
    return recorrido;
}


//buscar dato

int buscarD12(char name[100]){
    enlace2D=inicioD;
    if(enlace2D == NULL){
       return -1;
    }

    while(enlace2D != NULL){
        int res=0;
        res=strcasecmp(enlace2D->valor.path,name);
        if(res==0){
            return 1;
        }

        enlace2D=enlace2D->siguienteD1;
    }
    return 0;
}


char ParaMontarE(char name[100]){
    char y=' ';
    enlace2D=inicioD;
    if(enlace2D == NULL){
       return -1;
    }

    while(enlace2D != NULL){
        int res=0;
        res=strcasecmp(enlace2D->valor.path,name);
        if(res==0){
            return enlace2D->valor.name;
        }

        enlace2D=enlace2D->siguienteD1;
    }
    return y;
}

void eliminarDVerificarDiscoPart(int x)
{
    int i = 0;
    enlace2D= inicioD;
    if (inicioD == NULL)
    {
       printf("No hay particiones montadas que eliminar\n");
        return;
    }
    else
    {
        while (i < x)
        {
            enlace2D = enlace2D->siguienteD1;
            i++;
        }
        if(i==0){
             enlace2D=enlace2D->siguienteD1;
             inicioD=enlace2D;

        } else{
           tempD=enlace2D->siguienteD1;
           enlace2D=enlace2D->anteriorD1;
           enlace2D->siguienteD1=tempD;
        }
    }
}
