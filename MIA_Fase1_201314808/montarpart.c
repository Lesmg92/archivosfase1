#include "montarpart.h"

void insertarAdelanteLD(NodoLD x){
    enlace1= (struct nodoL*)malloc(1*sizeof(struct listaD));
    enlace1->anteriorD = NULL;
    enlace1->siguienteD = NULL;
    enlace1->valor=x;

    if (inicio == NULL){
     inicio = enlace1;
     enlace1=inicio;
 } else {
     enlace1->siguienteD= inicio;
     inicio->anteriorD= enlace1;
     inicio= enlace1;
 }
}


//metodo para imprimir
void printListMountedP(){
    enlace2=inicio;
    if(enlace2 == NULL){
       printf("No hay particiones montadas\n\n");
       return -1;
    }

    while(enlace2 != NULL){

            printf("name --> [%s]\n",enlace2->valor.name);
            printf("path --> [%s]\n",enlace2->valor.path);
            printf("id --> [%s]\n",enlace2->valor.id);
            enlace2=enlace2->siguienteD;
    }

}

//buscar dato
int buscarD(char name[100]){
    int encontrado=0;
    enlace2=inicio;
    if(enlace2 == NULL){
       //printf("la lista esta vacia");
       return -1;
    }

    while(enlace2 != NULL){
        //printf("WHILE WHILE \n");
        int res=0;
        res=strcasecmp(enlace2->valor.nameDisk,name);
        if(res==0){
            return encontrado;
        }

        enlace2=enlace2->siguienteD;
        encontrado++;
    }
    return 0;
}

int ParaElim(char name[100]){
    int encontrado=0;
    enlace2=inicio;
    if(enlace2 == NULL){
       //printf("la lista esta vacia");
       return -1;
    }

    while(enlace2 != NULL){
        //printf("WHILE WHILE \n");
        int res=0;
        res=strcasecmp(enlace2->valor.id,name);
        if(res==0){
            return encontrado;
        }

        enlace2=enlace2->siguienteD;
        encontrado++;
    }
    return 0;
}


int NumR(char name[100]){
    int encontrado=1;
    enlace2=inicio;
    if(enlace2 == NULL){
       //printf("la lista esta vacia");
       return 1;
    }

    while(enlace2 != NULL){
        //printf("WHILE WHILE \n");
        int res=0;
        res=strcasecmp(enlace2->valor.nameDisk,name);
        if(res==0){
            encontrado++;
        }

        enlace2=enlace2->siguienteD;
        
    }
    return encontrado;
}

void eliminarD(int x)
{
    int i = 0;
    enlace2= inicio;
    if (inicio == NULL)
    {
        printf("Error : No hay particiones montadas que eliminar\n");
        return;
    }
    else
    {
        while (i < x)
        {
            enlace2 = enlace2->siguienteD;
            i++;
        }
        if(i==0){

             enlace2=enlace2->siguienteD;
             inicio=enlace2;
        } else{
           temp=enlace2->siguienteD;
           enlace2=enlace2->anteriorD;
           enlace2->siguienteD=temp;
        }
    }
}

char* func(char name[300]) {
	char ui[300]="";
    enlace2=inicio;
    if(enlace2 == NULL){
       return 1;
    }

    while(enlace2 != NULL){
        int res=0;
        res=strcasecmp(enlace2->valor.id,name);
        if(res==0){
             return enlace2->valor.path;
        }

        enlace2=enlace2->siguienteD;
        
    }
    return " ";
}

char* Nunc(char name[300]) {
	char ui[300]="";
    enlace2=inicio;
    if(enlace2 == NULL){
       //printf("la lista esta vacia");
       return 1;
    }

    while(enlace2 != NULL){
        int res=0;
        res=strcasecmp(enlace2->valor.id,name);
        if(res==0){
             return enlace2->valor.name;
        }

        enlace2=enlace2->siguienteD;
        
    }
    return " ";
}