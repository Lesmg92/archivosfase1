#ifndef MONTARPART_H
#define MONTARPART_H
#include <stdio.h>

typedef struct NodoLD{
    int in;
    char path[300];
    char name[300];
    char id[300];
    char nameDisk[300];
} NodoLD;


struct listaD{
    NodoLD valor;
    struct listaD *anteriorD;
    struct listaD *siguienteD;
}*enlace1, *enlace2, *enlace3, *enlace4, *inicio,*temp;
//funciones a utilizar

FILE *grafo;
void crearNodoLD(int i);
void insertarAdelanteLD(NodoLD dato);
void imprimirLD();
void ordenarLD();
int buscarD(char name[100]);
void eliminarD(int u);
void imprimirNodoD();
void graph();
int ParaElim(char name[100]);
int NumR(char name[100]);
char* func(char name[300]);
char* Nunc(char name[300]);
#endif // MONTARPART_H
