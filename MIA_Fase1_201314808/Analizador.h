#ifndef ANALIZADOR_H
#define ANALIZADOR_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "GestorDiscos.h"

/*------------------------------------------------------------------------------------*/
/*---------------------------METODOS Y FUNCIONES AUXILIARES---------------------------*/
/*------------------------------------------------------------------------------------*/
bool startsWith(const char *pre, const char *str);
int buscarEspacios(char *token,char s);
char *quitarEspacios(char *tmp,const char *cadena);
void quitarUltimoEspacio(char str[256]);
void toLowerCase(char s[]);
void analyzeFile(char path[256]);
int verificarArchivoE(char path[300]);
void crearDirectorio(char path[300]);

/*------------------------------------------------------------------------------------*/
/*---------------------------VERIFICAR PARAMETROS CORRECTOS---------------------------*/
/*------------------------------------------------------------------------------------*/
bool nameDskOk(char s[]);
bool nameOk(char s[]);
bool pathOk(char *str);
bool unitOk(char car);
bool typeOk(char car);
bool fitOk(const char *str);
bool allocationtOk(const char *str);
bool deleteOk(const char *str);

/*------------------------------------------------------------------------------------*/
/*-------------------------------ACCIONES DE CADA COMANDO-----------------------------*/
/*------------------------------------------------------------------------------------*/
void goToComment(char str[256]);
void goToMkdisk(char str[256]);
void goToRmdisk(char str[256]);
void goToFdisk(char str[256]);
void goToMount(char str[256]);
void goToUnmount(char str[256]);
void goToDf(char str[256]);
void goToDu(char str[256]);
void goToMkfs(char str[256]);
void goToMkfile(char str[256]);
void goToRep(char str[256]);
void goToExec(char str[256]);

/*------------------------------------------------------------------------------------*/
/*-------------------------REDIRIGIR ORDEN RESPECTIVA---------------------------------*/
/*------------------------------------------------------------------------------------*/
void orden(char str[256]);


#endif