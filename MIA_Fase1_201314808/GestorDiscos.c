#include "GestorDiscos.h"

    VerFer direc;
    NodoLD mount;

/*------------------------------------------------------------------------------------*/
/*--------------------METODOS Y FUNCIONES PARA GESTION DE DISCOS----------------------*/
/*------------------------------------------------------------------------------------*/

void crearDisco(char dir[256],int sizeDisk,char tipo)
{
    FILE *archivoptr; //puntero del archivo
    archivoptr = fopen(dir,"wb+"); 
    if(archivoptr!=NULL) 
    {
        if(tipo=='m' || tipo=='M') 
            sizeDisk=sizeDisk*mb;
        else if(tipo=='k' || tipo=='K')
            sizeDisk=sizeDisk*kb;
        char *buffer = calloc(sizeDisk,sizeof(char)); //reservar ram
        fseek(archivoptr,0,SEEK_SET); // ptr a cero

        fwrite(buffer,sizeDisk,1,archivoptr); 
        fflush(archivoptr); 
        
        partition part1; // crea particion inicializando valores
        part1.part_status='0';
        part1.part_size=0;
        part1.part_start=0;
        part1.part_type=' ';
        part1.part_fit=' ';
        strcpy(part1.part_name,"");
        
        mbr mbrO;  // crea mbr asignando las particiones vacias
        mbrO.mbr_disk_signature = 1;
        mbrO.mbr_fecha_creacion = time(0);
        mbrO.mbr_partition_1=part1;
        mbrO.mbr_partition_2=part1;
        mbrO.mbr_partition_3=part1;
        mbrO.mbr_partition_4=part1;
        mbrO.mbr_tamano = sizeDisk;
        
        fseek(archivoptr,0,SEEK_SET); //regresa a 0 el ptr
        
        fwrite(&mbrO,sizeof(mbr),1,archivoptr);
        fclose(archivoptr); //escribe buffer y cierra
        fflush(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

void leerDisco(char dir[256])
{
    printf("---------------------\n");
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbrO;
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr
        
        if(mbrO.mbr_partition_1.part_status=='1' )
        {
            printf("%s\n",mbrO.mbr_partition_1.part_name);
        }
        else
            printf("Particion 1 vacia\n");
        if(mbrO.mbr_partition_2.part_status=='1' )
        {
            printf("%s\n",mbrO.mbr_partition_2.part_name);
        }
        else
            printf("Particion 2 vacia\n");
        if(mbrO.mbr_partition_3.part_status=='1' )
        {
            printf("%s\n",mbrO.mbr_partition_3.part_name);
        }
        else
            printf("Particion 3 vacia\n");
        if(mbrO.mbr_partition_4.part_status=='1' )
        {
            printf("%s\n",mbrO.mbr_partition_4.part_name);
        }
        else
            printf("Particion 4 vacia\n");

        fclose(archivoptr);
        fflush(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

void eliminarDisco(char dir[256])
{
    FILE *ptrFile;
    ptrFile = fopen(dir,"rb+");
    printf("**¿Desea eliminar el disco: [%s]?   (s|n)\n",dir);
    char conf[2];
    gets(conf);
    if(strcasecmp(conf,"s")==0 && ptrFile!=NULL)
    {
        fclose(ptrFile);
        if((remove(dir))==0)
            printf("El disco ha sido borrado\n");
        else
        printf("El disco NO ha sido borrado\n");
    }
    else
        printf("Error el archivo no existe\n");
}

/*------------------------------------------------------------------------------------*/
/*------------------METODOS Y FUNCIONES PARA GESTION DE PARTICIONES-------------------*/
/*------------------------------------------------------------------------------------*/

void modificarParticion(char dir[256],char nom[16],int modf){
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbrO;
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr
        
                int esp = mbrO.mbr_partition_1.part_start;
                int espaciolibre = 0;

        if(strcmp(mbrO.mbr_partition_1.part_name,nom) == 0)
            {
                
                if(mbrO.mbr_partition_2.part_status == '1'){
                    espaciolibre = mbrO.mbr_partition_2.part_start - esp;
                    if(espaciolibre>modf){
                        mbrO.mbr_partition_1.part_size = mbrO.mbr_partition_1.part_size + modf;
                    }
                }else if(mbrO.mbr_partition_3.part_status == '1'){
                    espaciolibre = mbrO.mbr_partition_3.part_start - esp;
                    if(espaciolibre>modf){
                        mbrO.mbr_partition_1.part_size = mbrO.mbr_partition_1.part_size + modf;
                    }
                }else if(mbrO.mbr_partition_4.part_status == '1'){
                    espaciolibre = mbrO.mbr_partition_3.part_start - esp;
                    if(espaciolibre>modf){
                        mbrO.mbr_partition_1.part_size = mbrO.mbr_partition_1.part_size + modf;
                    }
                }
            }
        else if(strcmp(mbrO.mbr_partition_2.part_name,nom) == 0)
            {
                if(mbrO.mbr_partition_3.part_status == '1'){
                    espaciolibre = mbrO.mbr_partition_3.part_start - esp;
                    if(espaciolibre>modf){
                        mbrO.mbr_partition_2.part_size = mbrO.mbr_partition_2.part_size + modf;
                    }
                }else if(mbrO.mbr_partition_4.part_status == '1'){
                    espaciolibre = mbrO.mbr_partition_3.part_start - esp;
                    if(espaciolibre>modf){
                        mbrO.mbr_partition_2.part_size = mbrO.mbr_partition_2.part_size + modf;
                    }
                }
            }
        else if(strcmp(mbrO.mbr_partition_3.part_name,nom) == 0)
            {
              if(mbrO.mbr_partition_4.part_status == '1'){
                    espaciolibre = mbrO.mbr_partition_3.part_start - esp;
                    if(espaciolibre>modf){
                        mbrO.mbr_partition_3.part_size = mbrO.mbr_partition_3.part_size + modf;
                    }
                }else{  
                int fin4 = mbrO.mbr_tamano - esp; 
                if(fin4 > modf){
                    mbrO.mbr_partition_3.part_size = mbrO.mbr_partition_3.part_size + modf;
                    }     
                    
                }
            }
        else if(strcmp(mbrO.mbr_partition_4.part_name,nom) == 0)
            {   
                int fin4 = mbrO.mbr_tamano - mbrO.mbr_partition_4.part_start + mbrO.mbr_partition_4.part_size; 
                if(fin4 > modf){
                    mbrO.mbr_partition_4.part_size = mbrO.mbr_partition_4.part_size + modf;
                    }      
            }
        else
            {
            printf("No existe particion con ese nombre\n");    
            }
            
        fseek(archivoptr,0,SEEK_SET);
        fwrite(&mbrO,sizeof(mbr),1,archivoptr);
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");

}

int getStartPart(const mbr mbr0, int sizeNPart, int pActual){
    
    int ptri = sizeof(mbr0) + 1;
    int ptrf = mbr0.mbr_tamano;
    
    partition parts[4];
    parts[0] = mbr0.mbr_partition_1;
    parts[1] = mbr0.mbr_partition_2;
    parts[2] = mbr0.mbr_partition_3;
    parts[3] = mbr0.mbr_partition_4;
    
    bool partsb[4];
    partsb[0] = mbr0.mbr_partition_1.part_status == '0';
    partsb[1] = mbr0.mbr_partition_2.part_status == '0';
    partsb[2] = mbr0.mbr_partition_3.part_status == '0';
    partsb[3] = mbr0.mbr_partition_4.part_status == '0';
   
    
   int contb = pActual-1;
   
   if(contb != 0){
       int pinicio = parts[contb-1].part_start;
       int pfin = parts[contb-1].part_size;
       ptri = pinicio + pfin +1 ;
       }
   
    while(contb < 3){
        if(partsb[contb]){
        bool cabe = true;
        int conta = contb + 1;
        while(conta<3 && cabe){
            if(!partsb[conta]){
                ptrf = parts[conta].part_start;
                int sizeSpace = ptrf - ptri;
                if(sizeSpace >= sizeNPart){
                    return ptri;
                } else {
                    cabe = false;
                    contb = conta;
                }
            }
            conta++;
        }
        if(cabe){
            if(!partsb[3]){
                ptrf = parts[conta].part_start;
                int sizeSpace = ptrf - ptri;
                if(sizeSpace >= sizeNPart){
                    return ptri;}
                }else {
                int sizeSpace = ptrf - ptri;
                if(sizeSpace >= sizeNPart){
                    return ptri;
                } 
            } 
        } else{
            ptrf = mbr0.mbr_tamano;
            }     
        contb++;
        }else{
            ptri = parts[contb].part_start + parts[contb].part_size + 1;
        }
    }     
   if(contb == 3 && partsb[3]){
            ptri = parts[contb-1].part_start + parts[contb-1].part_size + 1;
            int sizeSpace = ptrf - ptri;
            if(sizeSpace >= sizeNPart){
                    return ptri;
                }
        }
    return -1;
    printf("No hay espacio suficiente para agregar la particion\n");
}

void crearParticionAux(char dir[256],char nom[16],char tipo, char fit, int sizePart)
    {
        FILE *archivoptr;
            archivoptr = fopen(dir,"rb+");
            if(archivoptr!=NULL)
            {
                mbr mbrO;
                fseek(archivoptr,0,SEEK_SET); //ptr a 0
                fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr

                if(mbrO.mbr_partition_1.part_status == '0')
                {
                    int n = getStartPart(mbrO,sizePart,1);
                    if(n == -1)
                    {
                        printf("Error al crear la particion");
                    }
                    else
                    {
                         mbrO.mbr_partition_1.part_status='1';
                         mbrO.mbr_partition_1.part_type=tipo;
                         mbrO.mbr_partition_1.part_fit=fit;
                         mbrO.mbr_partition_1.part_start= n;
                         mbrO.mbr_partition_1.part_size=sizePart;
                         strcpy(mbrO.mbr_partition_1.part_name,nom);   
                    }       
                }
                else if(mbrO.mbr_partition_2.part_status == '0')
                {
                    int n = getStartPart(mbrO,sizePart,2);
                    if(n == -1){
                       printf("Error al crear la particion");
                    }
                    else
                    {
                         mbrO.mbr_partition_2.part_status='1';
                         mbrO.mbr_partition_2.part_type=tipo; 
                         mbrO.mbr_partition_2.part_fit=fit;
                         mbrO.mbr_partition_2.part_start=n;
                         mbrO.mbr_partition_2.part_size=sizePart;
                         strcpy(mbrO.mbr_partition_2.part_name,nom);
                    }
                }
                else if(mbrO.mbr_partition_3.part_status == '0')
                {
                    int n = getStartPart(mbrO,sizePart,3);
                    if(n == -1){
                        printf("Error al crear la particion");
                    }
                    else
                    {
                        mbrO.mbr_partition_3.part_status='1';
                        mbrO.mbr_partition_3.part_type=tipo; 
                        mbrO.mbr_partition_3.part_fit=fit;
                        mbrO.mbr_partition_3.part_start=n;
                        mbrO.mbr_partition_3.part_size=sizePart;
                        strcpy(mbrO.mbr_partition_3.part_name,nom);
                    }
                }
                else if(mbrO.mbr_partition_4.part_status == '0')
                {
                    int n = getStartPart(mbrO,sizePart,4);
                    if(n == -1){
                        printf("Error al crear la particion");
                    }
                    else
                    {
                        mbrO.mbr_partition_4.part_status='1';
                        mbrO.mbr_partition_4.part_type=tipo; 
                        mbrO.mbr_partition_4.part_fit=fit;
                        mbrO.mbr_partition_4.part_start=n;
                        mbrO.mbr_partition_4.part_size=sizePart;
                        strcpy(mbrO.mbr_partition_4.part_name,nom);    
                    }        
                }
                    else
                    {
                        printf("Las cuatro particiones estan ocupadas\n");    
                    }
                            
                        fseek(archivoptr,0,SEEK_SET);
                        fwrite(&mbrO,sizeof(mbr),1,archivoptr);
                        fclose(archivoptr);
                    }
                    else
                        printf("Error el archivo no existe\n");
    }

void crearParticion(char dir[256],char nom[16],char tipo, char fit, int sizePart)
{
    int n = verificarParticionLibre(dir);
    if(n!=0)
     {
        if(tipo=='p' || tipo=='P')
        {
            crearParticionAux(dir,nom,'p',fit,sizePart);
        }
        else if(tipo=='e' || tipo=='E')
        {
            int n = verificarExtendidaE(dir);
            if(n == 0)
                {
                    crearParticionAux(dir,nom,'e',fit,sizePart);
                    int inicioebr=0;
                    inicioebr=InicioExtendidas(dir)+1;
                    crearParticionLogica(dir,inicioebr);
                } 
        }
    } else if(tipo=='l' || tipo=='L')
        {
            int n = verificarExtendidaE(dir);
            if(n == 1)
                {
                    int inicio=0,siguienteP=0;
                    int tamInicia=0;
                    int prue=0;
                    tamInicia=InicioExtendidas(dir)+1;
                    int byteInicial=0;
                    byteInicial=TamDeEBR(dir,tamInicia);
                    int enlac=0;
                    while(prue!=1){
                    if(byteInicial==-1)
                    {
                        prue=1;
                    }
                    else
                    {
                         enlac=EnlazarEBR(dir,byteInicial);
                         byteInicial=TamDeEBR(dir,byteInicial);
                    }
                 }
              if(enlac==0)
              {
              enlac=InicioExtendidas(dir)+1;
              }
            
             if(byteInicial==-1)
            {
             inicio=enlac;
             siguienteP=inicio+sizePart+1;
             ModificarEBR(dir,fit,nom,inicio,siguienteP,sizePart,1);
             LeerEBR(dir,inicio);
             crearParticionLogica(dir,siguienteP);
             LeerEBR(dir,siguienteP);
             prue=0;
             }
             else
             {
                 prue=0;
              }     
            }
        } else {
            printf("No existe particion extendida para crear Lógicas\n");
            }
}


int verificarParticionLibre(char dir[256])
    {
        FILE *archivoptr;
        archivoptr = fopen(dir,"rb+");
        if(archivoptr!=NULL)
        {
            mbr mbrO;
            fseek(archivoptr,0,SEEK_SET); //ptr a 0
            fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr

            if(mbrO.mbr_partition_1.part_status == '0')
                {
                  return 1;      
                }
            else if(mbrO.mbr_partition_2.part_status == '0')
                {
                    return 2;
                }
            else if(mbrO.mbr_partition_3.part_status == '0')
                {
                    return 3;
                }
            else if(mbrO.mbr_partition_4.part_status == '0')
                {
                    return 4;    
                }
            else
                {
                printf("Las cuatro particiones estan ocupadas\n");  
                return 0;
                }
                
            fseek(archivoptr,0,SEEK_SET);
            fwrite(&mbrO,sizeof(mbr),1,archivoptr);
            fclose(archivoptr);
        }
        else
            printf("Error el archivo no existe\n");
            return 0;
    }

int verificarExtendidaE(char dir[256])
    {
        int nExt = 0;
        FILE *archivoptr;
        archivoptr = fopen(dir,"rb+");
        if(archivoptr!=NULL)
        {
            mbr mbrO;
            fseek(archivoptr,0,SEEK_SET); //ptr a 0
            fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr

            if(mbrO.mbr_partition_1.part_type == 'e')
                {
                  nExt++;      
                }
            else if(mbrO.mbr_partition_2.part_type == 'e')
                {
                  nExt++; 
                }
            else if(mbrO.mbr_partition_3.part_type == 'e')
                {
                  nExt++; 
                }
            else if(mbrO.mbr_partition_4.part_type == 'e')
                {
                  nExt++;    
                }
            else
                {
                return 0;
                printf("Las cuatro particiones estan ocupadas\n");  
                }
                
            fseek(archivoptr,0,SEEK_SET);
            fwrite(&mbrO,sizeof(mbr),1,archivoptr);
            fclose(archivoptr);
        }
        else{
            printf("Error el archivo no existe\n");
            return -1;
        }
            return nExt;
    }

void eliminarParticion(char dir[256],char nom[16])
{
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbrO;
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr

        if(strcmp(mbrO.mbr_partition_1.part_name,nom) == 0)
            {
                mbrO.mbr_partition_1.part_status='0';
                mbrO.mbr_partition_1.part_type= '0'; //fit
                mbrO.mbr_partition_1.part_start= 0 ;
                mbrO.mbr_partition_1.part_size= 0;
                strcpy(mbrO.mbr_partition_1.part_name, "");   
            }
        else if(strcmp(mbrO.mbr_partition_2.part_name,nom) == 0)
            {
                mbrO.mbr_partition_2.part_status='0';
                mbrO.mbr_partition_2.part_type= '0'; //fit
                mbrO.mbr_partition_2.part_start= 0 ;
                mbrO.mbr_partition_2.part_size= 0;
                strcpy(mbrO.mbr_partition_2.part_name,"");
            }
        else if(strcmp(mbrO.mbr_partition_3.part_name,nom) == 0)
            {
                mbrO.mbr_partition_3.part_status='0';
                mbrO.mbr_partition_3.part_type='0'; //fit
                mbrO.mbr_partition_3.part_start=0;
                mbrO.mbr_partition_3.part_size=0;
                strcpy(mbrO.mbr_partition_3.part_name,"");
            }
        else if(strcmp(mbrO.mbr_partition_4.part_name,nom) == 0)
            {
                mbrO.mbr_partition_4.part_status='0';
                mbrO.mbr_partition_4.part_type='0'; //fit
                mbrO.mbr_partition_4.part_start=0;
                mbrO.mbr_partition_4.part_size=0;
                strcpy(mbrO.mbr_partition_4.part_name,"");       
            }
        else
            {
            printf("No existe particion con ese nombre\n");    
            }
            
        fseek(archivoptr,0,SEEK_SET);
        fwrite(&mbrO,sizeof(mbr),1,archivoptr);
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}


int VerificarParticionesExtendidas(char ruta[256])
{
    int u=0,con=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbr1;
        fseek(archivoptr,0,SEEK_SET);
        fread(&mbr1,sizeof(mbr),1,archivoptr);
       // printf("EL TAMA;O DEL MBR ES %d\n",sizeof(mbr1));
       // printf("EL TAMA;O DEL disco ES %d\n",mbr1.mbr_tamano);
        if(mbr1.mbr_partition_1.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_1.part_type,"E");
          if(u==0)
            {
            con++;
            }
        }
        if(mbr1.mbr_partition_2.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_2.part_type,"E");
          if(u==0)
            {
            con++;
            }
        }
        if(mbr1.mbr_partition_3.part_status=='1' )
        {
           u = strcasecmp(mbr1.mbr_partition_3.part_type,"E");
          if(u==0)
            {
            con++;
            }
        }

        if(mbr1.mbr_partition_4.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_4.part_type,"E");
          if(u==0)
            {
            con++;
            }
        }

        return con;
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

int InicioExtendidas(char ruta[256])
{
    int u=0,con=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbr1;
        fseek(archivoptr,0,SEEK_SET);
        fread(&mbr1,sizeof(mbr),1,archivoptr);
        if(mbr1.mbr_partition_1.part_status=='1')
        {
            u = mbr1.mbr_partition_1.part_type;
          if( u == 'e' || u == 'E')
            {
            con=mbr1.mbr_partition_1.part_start;
            }
        }
        if(mbr1.mbr_partition_2.part_status=='1' )
        {
          u = mbr1.mbr_partition_2.part_type;
          if( u == 'e' || u == 'E')
            {
            con=mbr1.mbr_partition_2.part_start;
            }
        }
        if(mbr1.mbr_partition_3.part_status=='1' )
        {
          u = mbr1.mbr_partition_3.part_type;
          if( u == 'e' || u == 'E')
            {
            con=mbr1.mbr_partition_3.part_start;
            }
        }

        if(mbr1.mbr_partition_4.part_status=='1' )
        {
          u = mbr1.mbr_partition_4.part_type;
          if( u == 'e' || u == 'E')
            {
            con=mbr1.mbr_partition_4.part_start;
            }
        }

        fclose(archivoptr);
        return con;
    }
    else
        printf("Error el archivo no existe\n");
}

void crearParticionLogica(char ruta[256], int x){
     FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        ebr extendida;

        strcpy(extendida.part_name,"");
        extendida.part_fit = ' ';
        extendida.part_next=-1;
        extendida.part_size=0;
        extendida.part_start=x;
        extendida.part_status=1;
       
        fseek(archivoptr,x,SEEK_SET);
        fwrite(&extendida,sizeof(ebr),1,archivoptr);
        fclose(archivoptr);

    }
    else
        printf("Error el archivo no existe\n");
    
    }


int TamanoExtendidas(char ruta[256])
{
    int u=0,con=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbr1;
        fseek(archivoptr,0,SEEK_SET);
        fread(&mbr1,sizeof(mbr),1,archivoptr);
       // printf("EL TAMA;O DEL MBR ES %d\n",sizeof(mbr1));
       // printf("EL TAMA;O DEL disco ES %d\n",mbr1.mbr_tamano);
        if(mbr1.mbr_partition_1.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_1.part_type,"E");
          if(u==0)
            {
            con=mbr1.mbr_partition_1.part_size;
            }
        }
        if(mbr1.mbr_partition_2.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_2.part_type,"E");
          if(u==0)
            {
            con=mbr1.mbr_partition_2.part_size;
            }
        }
        if(mbr1.mbr_partition_3.part_status=='1' )
        {
           u = strcasecmp(mbr1.mbr_partition_3.part_type,"E");
          if(u==0)
            {
            con=mbr1.mbr_partition_3.part_size;
            }
        }

        if(mbr1.mbr_partition_4.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_4.part_type,"E");
          if(u==0)
            {
            con=mbr1.mbr_partition_4.part_size;
            }
        }

        return con;
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}


void ModificarEBR(char ruta[256],char fit,char name[16],int inicio,int next, int size, int status)
{
    int y=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        
        ebr extendida;
        fseek(archivoptr,inicio,SEEK_SET);
        fread(&extendida,sizeof(ebr),1,archivoptr);
        extendida.part_fit = fit;
        strcpy(extendida.part_name,name);
        extendida.part_next=next;
        extendida.part_size=size;
        extendida.part_start=inicio;
        extendida.part_status=status;

        fseek(archivoptr,inicio,SEEK_SET);
        fwrite(&extendida,sizeof(ebr),1,archivoptr);
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

void LeerEBR(char ruta[256],int inicio)
{
    int y=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        
        ebr extendida;
        fseek(archivoptr,inicio,SEEK_SET);
        fread(&extendida,sizeof(ebr),1,archivoptr);
        printf("-;fit %c \n",extendida.part_fit);
        printf("-:name %s \n",extendida.part_name);
        printf("-:start %d \n",extendida.part_start);
        printf("-:size %d \n",extendida.part_size);
        printf("-:status %d \n",extendida.part_status);
        printf("-:next %d \n",extendida.part_next);
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

int EnlazarEBR(char ruta[256],int inicio)
{
   int y=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        
        ebr extendida;
        fseek(archivoptr,inicio,SEEK_SET);
        fread(&extendida,sizeof(ebr),1,archivoptr);
        
        if(extendida.part_next==-1){
            printf("tam de ebr %d \n",extendida.part_start);
            return extendida.part_start;
        }else{
            EnlazarEBR(ruta,extendida.part_start);
        }
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}


int TamDeEBR(char ruta[256],int inicio)
{
    int y=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        
        ebr extendida;
        fseek(archivoptr,inicio,SEEK_SET);
        fread(&extendida,sizeof(ebr),1,archivoptr);
        return extendida.part_next;
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}



/*------------------------------------------------------------------------------------*/
/*--------------METODOS Y FUNCIONES PARA FORMATO DE PARTICIONES MONTADAS--------------*/
/*------------------------------------------------------------------------------------*/

char *getPartPath(char namePart[100])
    {
         if(NumR(namePart)==-1 || NumR(namePart)==0){
            printf("Error, no esta montada esta particion");
        }else{
         char *ruta=NULL;
            ruta=func(namePart);
            }
    }

void formatoParticion(char namePart[10], char type, char fs){
    
    char *dir = getPartPath(namePart);
    int posIniP = getPartStart(namePart);
    int sizeP = getPartSize(namePart);

    char bloque[50];
        
    int n = (sizeP)/(1+sizeof(bloque));
    int posBitMap = posIniP + sizeof(n)+1;
    
    char bitMap[n];
    int i = 0;
    for(i; i<n;i++){
            bitMap[i]='0';
        }
    
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        fseek(archivoptr, posIniP,SEEK_SET); 
        fwrite(&n,sizeof(n),1,archivoptr);
        fseek(archivoptr, posBitMap,SEEK_SET); 
        fwrite(&bitMap,sizeof(bitMap),1,archivoptr);
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}


void modificarFormatoPart(char namePart[10], int addInt, char unit){
    
    char *dir = getPartPath(namePart);
    int posIniP = getPartStart(namePart);
    int sizeP = getPartSize(namePart);
    
    int sizeSA;
    bool hasContent = false;
    int add;
    char bloque[50];
    
    if(unit=='m' || unit=='M') 
            add=addInt*mb;
        else if(unit=='k' || unit=='K')
            add=addInt*kb;
        else if(unit=='b' || unit=='B')
            add=addInt;
        else
            printf("no es valida la unidad, debe ser m,k,b");
 
        
        FILE *archivoptr;
                archivoptr = fopen(dir,"rb+");
                if(archivoptr!=NULL)
                {
                    fseek(archivoptr, posIniP,SEEK_SET); 
                    fread(&sizeSA,sizeof(sizeSA),1,archivoptr);
                    char bitMapAux[sizeSA];
                    fseek(archivoptr, posIniP+sizeof(sizeSA),SEEK_SET); 
                    fread(&bitMapAux,sizeof(bitMapAux),1,archivoptr);
                    int i = 0;
                    while(i<sizeSA){
                            if(bitMapAux[i] == '1'){
                                hasContent = true;
                            }
                            i++;
                        }
                    sizeSA = sizeSA * 50;
                    fseek(archivoptr,0,SEEK_SET); //ptr a 0
                    fclose(archivoptr);
                }
                else
                    printf("Error el archivo no existe\n");    
    

        int dif = sizeSA + add;
        
        if(hasContent)
            {
                printf("El sistema de archivos ya contiene archivos\n");
            }
        else
            {
            if(dif<55)
                {
                    printf("Error al cambiar tamaño particion resta mas de lo posible\n");
                }
            else if(dif>sizeP)
                {
                    printf("Error al cambiar tamaño particion suma mas de lo posible\n");
                }
            else
                {  
                    int n = (dif)/(1+sizeof(bloque));
                    int posBitMap = posIniP + sizeof(n);                
                    char bitMap[n];
                    int i = 0;
                        for(i; i<n;i++){
                               bitMap[i]='0';
                          }
                    FILE *archivoptr;
                    archivoptr = fopen(dir,"rb+");
                    if(archivoptr!=NULL)
                    {
                        fseek(archivoptr, posIniP,SEEK_SET); 
                        fwrite(&dif,sizeof(dif),1,archivoptr);
                        fseek(archivoptr, posBitMap,SEEK_SET); 
                        fwrite(&bitMap,sizeof(bitMap),1,archivoptr);
                        fseek(archivoptr,0,SEEK_SET); //ptr a 0
                        fclose(archivoptr);
                    }
                    else
                        printf("Error el archivo no existe\n");    
            } 
        }
}

/*------------------------------------------------------------------------------------*/
/*--------------METODOS Y FUNCIONES PARA GUARDAR EN PARTICION FORMATEADA--------------*/
/*------------------------------------------------------------------------------------*/

void guardarArchivo(char namePart[10], char nameFile[15], const char* cadena){
    
    char *dir = getPartPath(namePart);
    int posIniP = getPartStart(namePart);
    int sizeP = getPartSize(namePart);
    char fitP = 'b'; //getPartFit(namePart); 
     
     switch(fitP){
         case 'b':
            {
                bestFit(dir, nameFile, cadena, posIniP);
                break;
            }
         case 'f':
            {
                firstFit(dir, nameFile, cadena, posIniP);
                break;
            }
         case 'w':
            {
                worstFit(dir, nameFile, cadena, posIniP);
                break;
            }
        default:
            {
            
            }
    }
}

 
/*###################################### PRIMER AJUSTE #######################################*/
void firstFit(char dir[256], const char nameFile[15], const char* cadena, int posInicial){
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    int n;
    int sizeFile = strlen(cadena);
    
    // encuentro el numero de bloques necesarios para ingresar el archivo
    int bNeeded = 0;
    int divMod = sizeFile % 35;
        if(divMod == 0)
            {
                bNeeded = sizeFile / 35;
            }
        else
            {
                bNeeded = (sizeFile / 35)+1;
            }
    
    if(archivoptr!=NULL)
    {
        // leo el valor de n (posiciones del bitmap)
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial,SEEK_SET); 
        fread(&n,sizeof(n),1,archivoptr); //lee n
        
        // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
        char bitMap[n];
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n) + 1,SEEK_SET); 
        fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
        
        // recorro el vector para encontrar el primer espacio disponible con suficientes bloques
        int i = 0;
        int bEmpty = 0;  
        int lastUtilB = 0;
        
        while(i<n)
            {
                if(bitMap[i]=='0')
                    {
                        bEmpty++;
                        if(bEmpty == bNeeded)
                            {
                                lastUtilB = i; // obtengo la posicion del ultimo bloque de la serie de bloques donde cabe
                                i = n;
                            }
                    }
                i++;
            }
            
        // obtengo la primera posicion de la serie de bloques de la serie    
        int firstUtilB = lastUtilB - (bNeeded-1);
        i = firstUtilB;
        // lleno la serie de bloques en el bitMap con 1
        int cont = 0;
        for(cont; cont < bNeeded; cont++)
            {
                bitMap[i] = '1';
                i++;
            }
            
        // coloco el apuntador en la posicion inicial de la serie de bloques en particion 
        // y escribo el archivo 
        //int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + firstUtilB *50 + 1;
        char nameFileC[15];
        strcpy(nameFileC,nameFile);
        i = firstUtilB;
        cont = 0;
        int contB = 0;
        
        for(cont; cont < bNeeded; cont++)
            {
                char content[35];
                if(i == bNeeded - 1)
                    {
                        strcpy (content,"                                  ");
                        strcpy (content,cadena+contB);
                    }
                else
                    {
                        strncpy (content,cadena+contB,35);
                    }
                
                int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + i*50 + 1;
                fseek(archivoptr,0,SEEK_SET);
                fseek(archivoptr,posIniB,SEEK_SET);
                fwrite(&nameFileC, sizeof(nameFileC), 1, archivoptr);
                fseek(archivoptr,posIniB + sizeof(nameFileC),SEEK_SET);
                fwrite(&content, sizeof(content), 1, archivoptr);
                contB = contB + 35;
                i++;
            }  
        
        // reescribo el bitMap en el archivo
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1,SEEK_SET); 
        fwrite(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
        fclose(archivoptr);        
    }
    else
        printf("Error el archivo no existe\n");
}


/*###################################### MEJOR AJUSTE #####################################*/
void bestFit(char dir[256], char nameFile[15], char* cadena, int posInicial){
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    int n;
    int sizeFile = strlen(cadena);
    
    // encuentro el numero de bloques necesarios para ingresar el archivo
    int bNeeded = 0;
    int divMod = sizeFile % 35;
        if(divMod == 0)
            {
                bNeeded = sizeFile / 35;
            }
        else
            {
                bNeeded = (sizeFile / 35)+1;
            }
    
    
    if(archivoptr!=NULL)
    {
        // leo el valor de n (posiciones del bitmap)
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial,SEEK_SET); 
        fread(&n,sizeof(n),1,archivoptr); //lee n
        // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
        char bitMap[n];
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1, SEEK_SET); 
        fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap

        int serieUtilB[n][2];
        // recorro el vector para encontrar los espacios en donde quepa
        int block = 0;
        int pos = 0;
        int bEmpty = 0;  
        int lastUtilB = 0;
        int nVacios = 0;
        // lleno la matriz con los valores de: [inicioSerieUtil][numeroDeBloques]
        while(block < n)
            {
                if(bitMap[block] == '0')
                        nVacios++;
                else
                    {
                        serieUtilB[pos][0] = block - nVacios;
                        serieUtilB[pos][1] = nVacios;
                        pos++;
                        nVacios = 0;
                    }
                block++;
            }
        if(nVacios == n){
            serieUtilB[0][0]= 0;
            serieUtilB[0][1]= n;
            }
        // recorro la matriz para encontrar el menor diferente de 0
        int i = 0;
        int posMin = 0;
        int min = serieUtilB[0][1];
        
        for(i; i<n;i++){
                    int valNow = serieUtilB[i][1];
                    if(valNow<min && valNow !=0 && bNeeded)
                        {
                            min = valNow;
                            posMin = i;
                        }
            }
        
        // obtengo la primera posicion de la serie de bloques de la serie    
        int firstUtilB = posMin;
        i = firstUtilB;
        // lleno la serie de bloques en el bitMap con 1
        int cont = 0;
        for(cont; cont < bNeeded; cont++)
            {
                bitMap[i] = '1';
                i++;
            }
            
        // coloco el apuntador en la posicion inicial de la serie de bloques en particion 
        // y escribo el archivo 
        //int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + firstUtilB *50 + 1;
        char nameFileC[15];
        strcpy(nameFileC,nameFile);
        i = firstUtilB;
        cont = 0;
        int contB = 0;
        
        for(cont; cont < bNeeded; cont++)
            {
                char content[35];
                if(i == bNeeded - 1)
                    {
                        strcpy (content,"                                  ");
                        strcpy (content,cadena+contB);
                    }
                else
                    {
                        strncpy (content,cadena+contB,35);
                    }
                
                int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + i*50 + 1;
                fseek(archivoptr,0,SEEK_SET);
                fseek(archivoptr,posIniB,SEEK_SET);
                fwrite(&nameFileC, sizeof(nameFileC), 1, archivoptr);
                fseek(archivoptr,posIniB + sizeof(nameFileC),SEEK_SET);
                fwrite(&content, sizeof(content), 1, archivoptr);
                contB = contB + 35;
                i++;
            }  
        
        // reescribo el bitMap en el archivo
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1,SEEK_SET); 
        fwrite(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
        fclose(archivoptr);           
    }
    else
        printf("Error el archivo no existe\n");
    
}

/*###################################### PEOR AJUSTE ######################################*/
void worstFit(char dir[256], char nameFile[15], const char* cadena, int posInicial){
     FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    int n;
    int sizeFile = strlen(cadena);
    
    // encuentro el numero de bloques necesarios para ingresar el archivo
    int bNeeded = 0;
    int divMod = sizeFile % 35;
        if(divMod == 0)
            {
                bNeeded = sizeFile / 35;
            }
        else
            {
                bNeeded = (sizeFile / 35)+1;
            }
    
    
    if(archivoptr!=NULL)
    {
        // leo el valor de n (posiciones del bitmap)
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial,SEEK_SET); 
        fread(&n,sizeof(n),1,archivoptr); //lee n
        // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
        char bitMap[n];
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1, SEEK_SET); 
        fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap

        int serieUtilB[n][2];
        // recorro el vector para encontrar los espacios en donde quepa
        int block = 0;
        int pos = 0;
        int bEmpty = 0;  
        int lastUtilB = 0;
        int nVacios = 0;
        // lleno la matriz con los valores de: [inicioSerieUtil][numeroDeBloques]
        while(block < n)
            {
                if(bitMap[block] == '0')
                        nVacios++;
                else
                    {
                        serieUtilB[pos][0] = block - nVacios;
                        serieUtilB[pos][1] = nVacios;
                        pos++;
                        nVacios = 0;
                    }
                block++;
            }
        if(nVacios == n){
            serieUtilB[0][0]= 0;
            serieUtilB[0][1]= n;
            }
        // recorro la matriz para encontrar el menor diferente de 0
        int i = 0;
        int posMin = 0;
        int max = serieUtilB[0][1];
        
        for(i; i<n;i++){
                    int valNow = serieUtilB[i][1];
                    if(valNow>max && valNow !=0 && bNeeded)
                        {
                            max = valNow;
                            posMin = i;
                        }
            }
        
        // obtengo la primera posicion de la serie de bloques de la serie    
        int firstUtilB = posMin;
        i = firstUtilB;
        // lleno la serie de bloques en el bitMap con 1
        int cont = 0;
        for(cont; cont < bNeeded; cont++)
            {
                bitMap[i] = '1';
                i++;
            }
            
        // coloco el apuntador en la posicion inicial de la serie de bloques en particion 
        // y escribo el archivo 
        //int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + firstUtilB *50 + 1;
        char nameFileC[15];
        strcpy(nameFileC,nameFile);
        i = firstUtilB;
        cont = 0;
        int contB = 0;
        
        for(cont; cont < bNeeded; cont++)
            {
                char content[35];
                if(i == bNeeded - 1)
                    {
                        strcpy (content,"                                  ");
                        strcpy (content,cadena+contB);
                    }
                else
                    {
                        strncpy (content,cadena+contB,35);
                    }
                
                int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + i*50 + 1;
                fseek(archivoptr,0,SEEK_SET);
                fseek(archivoptr,posIniB,SEEK_SET);
                fwrite(&nameFileC, sizeof(nameFileC), 1, archivoptr);
                fseek(archivoptr,posIniB + sizeof(nameFileC),SEEK_SET);
                fwrite(&content, sizeof(content), 1, archivoptr);
                contB = contB + 35;
                i++;
            }  
        
        // reescribo el bitMap en el archivo
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1,SEEK_SET); 
        fwrite(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
        fclose(archivoptr);           
    }
    else
        printf("Error el archivo no existe\n");
}

/*------------------------------------------------------------------------------------*/
/*--------------------METODOS Y FUNCIONES PARA REPORTAR ARCHIVOS----------------------*/
/*------------------------------------------------------------------------------------*/

void repBitMap(char namePart[10], char outPath[256])
    {
        char *dir = getPartPath(namePart);
        int posIniP = getPartStart(namePart);
        
        FILE *archivoptr;
        archivoptr = fopen(dir,"rb+");
        int n;
        
        FILE *fp;
        fp = fopen(outPath,"w"); //use outpath   // Open File in Write Mode
                
        if(archivoptr!=NULL)
            {
                fseek(archivoptr,0,SEEK_SET); //ptr a 0
                fseek(archivoptr,posIniP,SEEK_SET); 
                fread(&n,sizeof(n),1,archivoptr); //lee n
                // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
                char bitMap[n];
                fseek(archivoptr,posIniP + sizeof(n) +1,SEEK_SET); 
                fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
                fclose(archivoptr);
                fflush(archivoptr);
                
                int modLineas = n%20;
                int lineas = n/20;
                if(modLineas!=0)
                    lineas++;
                int i = 0;
                int cLinea =0;
                for(i; i<lineas;i++)
                    {
                        char tLinea[21];
                        strcpy(tLinea, bitMap+cLinea);
                        fflush(fp);
                        fprintf(fp, "%s\n", tLinea);
                        cLinea+20;
                    }
               fclose(fp); 
               char comandoOpen[256];
               char comandoStart[15] = "gnome-open ";
               strcpy(comandoOpen,comandoStart);
               strcat(comandoOpen,outPath);
             //  system(comandoOpen);
            }
        else
            printf("Error el archivo no existe\n");
    }

void repBloques(char namePart[10], char outPath[256])
    {
        char *dir = getPartPath(namePart);
        int posIniP = getPartStart(namePart);
    
        generaDotBloques(dir, posIniP,outPath);
    }


void generaDotBloques(char diskPath[256], int posIni, char outPath[256]) 
    {
        FILE *fp;
        char outPatDot[256];
        strcpy(outPatDot,outPath);
        strcat(outPatDot,".dot");
        fp = fopen(outPatDot,"w");   // Open File in Write Mode
        
        
        char outPatPng[256];
        strcpy(outPatPng,outPath);
        strcat(outPatPng,".png");  
        
        fprintf(fp,"digraph G { \n\trankdir = LR;\n\tnode [shape=record];\n\n\t");
        contentDotBloques(fp, posIni, diskPath );
        fprintf(fp,"}");
        fclose(fp);
        
        char comandoDot[100]= "dot ";
        
        strcat(comandoDot, outPatDot);
        strcat(comandoDot, " -o ");
        strcat(comandoDot, outPatPng);
        strcat(comandoDot, " -Tpng ");
        
        system(comandoDot); 

        char comandoOpen[256]= "gnome-open ";
        strcat(comandoOpen,outPath);
        strcat(comandoOpen, ".png");
        
        system(comandoOpen);
    }
    
void contentDotBloques(FILE *stream, int posIni, char dir[256]){
        FILE *archivoptr;
        archivoptr = fopen(dir,"rb+");
        int n;
        
        if(archivoptr!=NULL)
        {
            fseek(archivoptr,0,SEEK_SET); //ptr a 0
            fseek(archivoptr,posIni,SEEK_SET); 
            fread(&n,sizeof(n),1,archivoptr); //lee n
            // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
            char bitMap[n];
            fseek(archivoptr,posIni + sizeof(n) +1,SEEK_SET); 
            fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
            
            int posIniB = posIni+sizeof(n)+sizeof(bitMap)+1;
            int i = 0;
            char prevName[15] = "";
            for(i; i<n;i++)
                {   
                    if(bitMap[i] == '1')
                    {                            
                        char nameFile[15];
                        fseek(archivoptr,0,SEEK_SET); //ptr a 0
                        fseek(archivoptr,posIniB + (i*50),SEEK_SET); 
                        fread(&nameFile,sizeof(nameFile),1,archivoptr); //lee n
                        
                        char contentFile[35];
                        fseek(archivoptr,0,SEEK_SET); //ptr a 0
                        fseek(archivoptr,posIniB + (i*50) + sizeof(nameFile),SEEK_SET); 
                        fread(&contentFile,sizeof(contentFile),1,archivoptr); //lee n
                        
                        fprintf(stream, "bloque%d [label=\"Bloque %d|%s|%s\"];\n\t", i,i,nameFile,contentFile);
                        if(strcmp(prevName, nameFile) == 0)
                            {
                                fprintf(stream, "bloque%d -> bloque%d;\n\t", i-1,i);
                            }
                        strcpy(prevName,nameFile);
                    }
                }
                fclose(archivoptr);  
        }
        else
            printf("Error el archivo no existe\n");
    }



void repDSK(char id[50], char ruta[256])
{
    char *dir = getPartPath(id);
                
   FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbr1;
        int inip[4] = {0,0,0,0};
        int sizep[4] = {0,0,0,0};
        fseek(archivoptr,0,SEEK_SET);
        fread(&mbr1,sizeof(mbr),1,archivoptr);
        fclose(archivoptr);
        fflush(archivoptr);
      
        //Para particion4
        if(mbr1.mbr_partition_1.part_status=='1' )
        {
        inip[0] = mbr1.mbr_partition_1.part_start;
        sizep[0] = mbr1.mbr_partition_1.part_size;
        }
        
        if(mbr1.mbr_partition_2.part_status=='1' )
        {
        inip[1] = mbr1.mbr_partition_2.part_start;
        sizep[1] = mbr1.mbr_partition_2.part_size;
        }
        
        if(mbr1.mbr_partition_3.part_status=='1' )
        {
        inip[2] = mbr1.mbr_partition_3.part_start;
        sizep[2] = mbr1.mbr_partition_3.part_size;
        }
        
        if(mbr1.mbr_partition_4.part_status=='1' )
        {
        inip[3] = mbr1.mbr_partition_4.part_start;
        sizep[3] = mbr1.mbr_partition_4.part_size;
        }
        
        FILE *grafo = fopen("/home/leslie/Escritorio/disk.dot","w+");        
        fprintf(grafo,"digraph{ \n");
        fprintf(grafo,"\t  fontsize = 20;\n");
        fprintf(grafo,"\t  node [shape=record];\n\n");
        fprintf(grafo,"\t  subgraph cluster{\n");
        int sizeMbr = sizeof(mbr1);
        char stat1 = mbr1.mbr_partition_1.part_status;
        char stat2 = mbr1.mbr_partition_2.part_status;
        char stat3 = mbr1.mbr_partition_3.part_status;
        char stat4 = mbr1.mbr_partition_4.part_status;
        fflush(grafo);
        fprintf(grafo,"\t parti [label=\"MBR\\nEspacio=%d \\n P1= %c \\n P2= %c \\n P3= %c \\n P4=%c |",sizeMbr,stat1,stat2,stat3,stat4);

        int i = 0;
        int c0 = 0;
        int iniVacias = 0;
        int finVacias = 0;
        while(i<4)
        {
            if(inip[i]!=0)
            {
                c0 = 0;
                iniVacias = 0;
                finVacias = 0;
                char partiName[20];
                char tipoPart;
                switch(i){
                    case 0:{
                        strcpy(partiName,mbr1.mbr_partition_1.part_name);
                        tipoPart = mbr1.mbr_partition_1.part_type;
                        fprintf(grafo," Nombre particion:%s \\n Inicio:%d\\nTamano:%d \\Tipo:%c |",partiName,inip[i],sizep[i],tipoPart);
                        break;}
                    case 1:{
                        strcpy(partiName,mbr1.mbr_partition_2.part_name);
                        tipoPart = mbr1.mbr_partition_2.part_type;
                        fprintf(grafo," Nombre particion:%s \\n Inicio:%d\\nTamano:%d \\Tipo:%c |",partiName,inip[i],sizep[i],tipoPart);
                        break;}
                    case 2:{
                        strcpy(partiName,mbr1.mbr_partition_3.part_name);
                        tipoPart = mbr1.mbr_partition_3.part_type;
                        fprintf(grafo," Nombre particion:%s \\n Inicio:%d\\nTamano:%d \\Tipo:%c  |",partiName,inip[i],sizep[i],tipoPart);
                        break;}
                    case 3:{
                        strcpy(partiName,mbr1.mbr_partition_4.part_name);
                        tipoPart = mbr1.mbr_partition_4.part_type;
                        fprintf(grafo," Nombre particion:%s \\n Inicio:%d\\nTamano:%d \\Tipo:%c ",partiName,inip[i],sizep[i],tipoPart);
                        break;}
                    }
            }else{
                    if(c0==0){
                        if(i-1 != 0)
                        {
                            iniVacias = inip[i-1]+sizep[i-1]+1;
                        }
                        else
                        {
                            iniVacias = sizeof(mbr1) + 1;
                        }
                        if(i == 3)
                        {
                            iniVacias = inip[2] + sizep[2] + 1;
                            finVacias = mbr1.mbr_tamano;
                            fprintf(grafo," Espacio Vacio:\\nInicio:%d\\nTamano:%d", iniVacias, finVacias-iniVacias);
                            finVacias = 0;
                        }
                       else 
                        {
                        if(i+1 < 4)
                            {
                            if(inip[i+1] != 0)
                                {
                                    finVacias = inip[i+1]-1;
                                }
                            }
                        }
                    }
                    if(finVacias != 0){
                       fprintf(grafo," Espacio Vacio:\\nInicio:%d\\nTamano:%d |", iniVacias, finVacias-iniVacias);
                        }
                    c0++;
                }
        i++;
        }
        fprintf(grafo, "\"];\n");
        fprintf(grafo,"\t label = \"Reporte de Disco\";\n");
        fprintf(grafo,"\t  } \n");        
        fprintf(grafo,"\t  } \n");             
        fclose(grafo);
    }
    

        system("dot -Tpng /home/leslie/Escritorio/disk.dot -o /home/leslie/Escritorio/disk.png");
}
        
int next1(char ruta[256],int inicio)
{
    int y=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        
        ebr extendida;
        fseek(archivoptr,inicio,SEEK_SET);
        fread(&extendida,sizeof(ebr),1,archivoptr);
        return extendida.part_next;
        fclose(archivoptr);
    }
    else
        printf("\n");
}

int size1(char ruta[256],int inicio)
{
    int y=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        
        ebr extendida;
        fseek(archivoptr,inicio,SEEK_SET);
        fread(&extendida,sizeof(ebr),1,archivoptr);
        return extendida.part_size;
        fclose(archivoptr);
    }
    else
        printf("\n");
}

void name21(char ruta[256],int inicio, char ret[16])
{
    int y=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        
        ebr extendida;
        fseek(archivoptr,inicio,SEEK_SET);
        fread(&extendida,sizeof(ebr),1,archivoptr);
        strcpy(ret,extendida.part_name);
        fclose(archivoptr);
    }
    else
        printf("\n");
}
    
        
void repMBR(char id[50])
{
    char *dire = getPartPath(id);
    
    mbr mbr1;
    FILE *archivoptr;
    archivoptr = fopen(dire,"rb+");
    if(archivoptr!=NULL)
    {
    fseek(archivoptr,0,SEEK_SET);
    fread(&mbr1,sizeof(mbr),1,archivoptr);
    fclose(archivoptr);
    fflush(archivoptr);
    }   
    char *dir =  getPartPath(id);
    FILE *grafo = fopen("/home/leslie/Escritorio/ebr.dot","w+");
    fprintf(grafo,"digraph{ \n");
    fprintf(grafo,"\t  rankdir=LR;\n");
    fprintf(grafo,"\t  fontsize = 20;\n");
    fprintf(grafo,"\t  ranksep = 0.3;\n");
    fprintf(grafo,"\t  node [shape=record];\n\n");
    
    fprintf(grafo,"\t  subgraph cluster_0{\n");
    int uo=0;
    char *yu;
    int inicio=0,siguienteP=0;
    int tamInicia=0;
    int prue=0;
    tamInicia=InicioExtendidas(dir)+1;
    int byteInicial=0;
    int size12=0;
    int next12=0;
    byteInicial=TamDeEBR(dir,tamInicia);

    size12=size1(dir,tamInicia);
    next12=next1(dir,tamInicia);
    char rec[16];
    name21(dir,tamInicia,rec);
 
    fprintf(grafo,"\t lider%d [ label = \" Posicion Inicial = %d \\n Siguiente = %d \\n Size = %d \\n Nombre= %s \" ];\n",uo,tamInicia,next12,size12,rec);             uo++;
    int yamearte=0;
    while(prue!=1){
        if(byteInicial==-1){
           prue=1;
        } 
        else
        {
            size12=size1(dir,byteInicial);
            next12=next1(dir,byteInicial);
            byteInicial=TamDeEBR(dir,byteInicial);
            name21(dir,byteInicial,rec);
            
            fprintf(grafo,"\t lider%d [ label = \" Posicion Inicial = %d \\n Siguiente = %d \\n Size = %d \\n Nombre= %s \" ];\n",uo,byteInicial,next12,size12,rec);
            uo++;
        }                   
    }          
        int xwq=0;
        for(xwq=0; xwq<uo-1;xwq++){
            fprintf(grafo,"\t  lider%d -> lider%d ;\n",xwq,xwq+1);
        }
            
        fprintf(grafo,"\t label = \"EBR\";\n");
        fprintf(grafo,"\t  } \n");
        
        int sizeMbr = mbr1.mbr_tamano;
        char stat1 = mbr1.mbr_partition_1.part_status;
        char stat2 = mbr1.mbr_partition_2.part_status;
        char stat3 = mbr1.mbr_partition_3.part_status;
        char stat4 = mbr1.mbr_partition_4.part_status;
        
        char nam1[16]; 
        strcpy(nam1,mbr1.mbr_partition_1.part_name);
        
        char nam2[16];
        strcpy(nam2,mbr1.mbr_partition_2.part_name);
        
        char nam3[16]; 
        strcpy(nam3,mbr1.mbr_partition_3.part_name);
        
        char nam4[16]; 
        strcpy(nam4,mbr1.mbr_partition_4.part_name);
        
        int si1 = mbr1.mbr_partition_1.part_size;
        int si2 = mbr1.mbr_partition_2.part_size;
        int si3 = mbr1.mbr_partition_3.part_size;
        int si4 = mbr1.mbr_partition_4.part_size;
        
        fprintf(grafo,"\t  subgraph cluster_1{\n");
        fprintf(grafo,"\t parti [label=\" Espacio=%d | Nombre1 = %s \\n Estado1= %c \\n Size1 = %d | Nombre2 = %s \\n Estado2= %c \\n Size2 = %d | Nombre3 = %s \\n Estado3= %c \\n Size3 = %d | Nombre4 = %s \\n Estado4=%c \\n Size4 = %d \"]\n",sizeMbr,nam1,stat1,si1,nam2,stat2,si2,nam3,stat3,si3,nam4,stat4,si4);
        fprintf(grafo,"\t label = \"MBR\";\n");
        
        fprintf(grafo,"\t  } \n");
        fprintf(grafo,"\t  } \n");
        fclose(grafo);
        
        
        
        system("dot -Tpng /home/leslie/Escritorio/ebr.dot -o /home/leslie/Escritorio/ebr.png");

}
         
/*------------------------------------------------------------------------------------*/
/*-----------------------METODOS Y FUNCIONES PARA MOUNT-------------------------------*/
/*------------------------------------------------------------------------------------*/

//Verifica Si existe un nombre
int NombreDeParticiones(char ruta[256], char name[300])
{
    int u=0,con=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbr1;
        fseek(archivoptr,0,SEEK_SET);
        fread(&mbr1,sizeof(mbr),1,archivoptr);
        if(mbr1.mbr_partition_1.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_1.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_1.part_size;
            }
        }
        if(mbr1.mbr_partition_2.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_2.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_2.part_size;
            }
        }
        if(mbr1.mbr_partition_3.part_status=='1' )
        {
           u = strcasecmp(mbr1.mbr_partition_3.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_3.part_size;
            }
        }

        if(mbr1.mbr_partition_4.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_4.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_4.part_size;
            }
        }
        
        fclose(archivoptr);
        return con;
    }
    else
        printf("Error el archivo no existe\n");
}

int StartParticiones(char ruta[256], char name[300])
{
    int u=0,con=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbr1;
        fseek(archivoptr,0,SEEK_SET);
        fread(&mbr1,sizeof(mbr),1,archivoptr);
        if(mbr1.mbr_partition_1.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_1.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_1.part_start;
            }
        }
        if(mbr1.mbr_partition_2.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_2.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_2.part_start;
            }
        }
        if(mbr1.mbr_partition_3.part_status=='1' )
        {
           u = strcasecmp(mbr1.mbr_partition_3.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_3.part_start;
            }
        }

        if(mbr1.mbr_partition_4.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_4.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_4.part_start;
            }
        }

        fclose(archivoptr);
        return con;
    }
    else
        printf("Error el archivo no existe\n");
}

 //METODO PARA MONTAR
void mountPart(char path[300], char name[300]){
    strcpy(mount.path,path);
    char letras[27]="abcdefghijklmnopqrstuvwyxz";
    char valor[300]="";
    char *temp;
    char *rest =path;
    while((temp = strtok_r(rest, "/", &rest)))
    {
           strcpy(valor,temp);
    }
     int x=0;
     x=buscarD12(valor);
     printf("En x: %d\n",x);
     if(x==-1){
         direc.name=letras[0];
        strcpy(direc.path,valor);
         VerificarDiscoPart(direc);
     }else if(x==0){
         int u=0;
         u=tamanoLista();
         direc.name=letras[u];
        strcpy(direc.path,valor);
         VerificarDiscoPart(direc);
     }
     
             int y=0;
        y=buscarD(valor);
         printf("En y: %d\n",y);
        if(y==-1 || y==0){
            strcpy(mount.name,name);
            strcpy(mount.nameDisk,valor);
            char ui[50]="",montado[50]="";
            sprintf(ui, "%d",NumR(valor));
            printf("En el ui=%s\n",ui);
            montado[0]='v';
            montado[1]='d';
            montado[2]=ParaMontarE(valor);
            strcat(montado,ui);
       strcpy(mount.id,montado);
            insertarAdelanteLD(mount);

        }
        
           strcpy(path,mount.path);
}

void Uumount_Metodo(char name[300]){
    char valor[300]="";
    char *temp;
    char *rest =name;
    while((temp = strtok_r(rest, ",", &rest)))
    {
           strcpy(valor,temp);
    }
    if(ParaElim(valor)==0 ||ParaElim(valor)==-1 ){
    printf("Error No hay ninguna particion montada con ese id\n");
    }else{
       eliminarD(ParaElim(valor)); 
    }
    
        }
        
int getPartSize(char namePart[100]){
        if(NumR(namePart)==-1 || NumR(namePart)==0){
            printf("Error, no esta montada esta particion\n");
        }else{
            char *ruta=NULL, *name=NULL;
            ruta=func(namePart);
            name=Nunc(namePart);
            return NombreDeParticiones(ruta,name);
            }
             
     }
     
int getPartStart(char namePart[100]){
        if(NumR(namePart)==-1 || NumR(namePart)==0){
            printf("Error, no esta montada esta particion\n");
        }else{
            char *ruta=NULL, *name=NULL;
            ruta=func(namePart);
            name=Nunc(namePart);
            return StartParticiones(ruta,name);
            }
        }
           