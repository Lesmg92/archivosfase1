#ifndef ANALIZADOR_H
#define ANALIZADOR_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "GestorDiscos.h"

/*------------------------------------------------------------------------------------*/
/*---------------------------METODOS Y FUNCIONES AUXILIARES---------------------------*/
/*------------------------------------------------------------------------------------*/
bool startsWith(const char *pre, const char *str);
int buscarEspacios(char *token,char s);
char *quitarEspacios(char *tmp,const char *cadena);
void quitarUltimoEspacio(char *str);
void toLowerCase(char s[]);
void analyzeFile(char *path);

/*------------------------------------------------------------------------------------*/
/*---------------------------VERIFICAR PARAMETROS CORRECTOS---------------------------*/
/*------------------------------------------------------------------------------------*/
bool nameDskOk(char s[]);
bool nameOk(char s[]);
bool pathOk(char *str);
bool unitOk(char car);
bool typeOk(char car);
bool fitOk(const char *str);
bool allocationtOk(const char *str);
bool deleteOk(const char *str);

/*------------------------------------------------------------------------------------*/
/*-------------------------------ACCIONES DE CADA COMANDO-----------------------------*/
/*------------------------------------------------------------------------------------*/
void goToComment(const char *str);
void goToMkdisk(const char *str);
void goToRmdisk(const char *str);
void goToFdisk(const char *str);
void goToMount(const char *str);
void goToUnmount(const char *str);
void goToDf(const char *str);
void goToDu(const char *str);
void goToMkfs(const char *str);
void goToMkfile(const char *str);
void goToRep(const char *str);
void goToExec(const char *str);

/*------------------------------------------------------------------------------------*/
/*-------------------------REDIRIGIR ORDEN RESPECTIVA---------------------------------*/
/*------------------------------------------------------------------------------------*/
int orden(const char *str);


#endif