#ifndef GESTORDISCOS_H
#define GESTORDISCOS_H

#include <stdlib.h>

#include <strings.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <time.h> 
#include "montarpart.h"
#include <stdio.h>
#include "validaciones.h"

#define kb 1024 
#define mb 1048576 
typedef struct partition partition; 
typedef struct mbr mbr;
/*------------------------------------------------------------------------------------*/
/*--------------------------ESTRUCTURAS PARA DISCO Y PARTICION------------------------*/
/*------------------------------------------------------------------------------------*/

struct partition
{
    char part_status;
    char part_type;
    char part_fit;
    int part_start;
    int part_size;
    char part_name[16];
};

struct mbr
{
    int mbr_tamano;
    time_t mbr_fecha_creacion;
    int mbr_disk_signature;
    struct partition mbr_partition_1;
    struct partition mbr_partition_2;
    struct partition mbr_partition_3;
    struct partition mbr_partition_4;
};

/*------------------------------------------------------------------------------------*/
/*--------------------METODOS Y FUNCIONES PARA GESTION DE DISCOS----------------------*/
/*------------------------------------------------------------------------------------*/
void crearDisco(char dir[256],int sizeDisk,char tipo);
void leerDisco(char dir[256]);
void eliminarDisco(const char dir[256]);

/*------------------------------------------------------------------------------------*/
/*------------------METODOS Y FUNCIONES PARA GESTION DE PARTICIONES-------------------*/
/*------------------------------------------------------------------------------------*/
int getStartPart(const mbr mbr0, int sizeNPart, int pActual);
void crearParticion(char dir[256],char nom[16],char tipo, char fit, int sizePart);
void eliminarParticion(char dir[256],char nom[16]);

/*------------------------------------------------------------------------------------*/
/*--------------METODOS Y FUNCIONES PARA FORMATO DE PARTICIONES MONTADAS--------------*/
/*------------------------------------------------------------------------------------*/
char *getPartPath(char namePart[100]);
void formatoParticion(char namePart[10], char type, char fs);
void modificarFormatoPart(char namePart[10], int addInt, char unit);

/*------------------------------------------------------------------------------------*/
/*--------------METODOS Y FUNCIONES PARA GUARDAR EN PARTICION FORMATEADA--------------*/
/*------------------------------------------------------------------------------------*/
void guardarArchivo(char namePart[10], char nameFile[15], const char* cadena);
void firstFit(char dir[256], const char nameFile[15], const char* cadena, int posInicial);
void bestFit(char dir[256], char nameFile[15], char* cadena, int posInicial);
void worstFit(char dir[256], char nameFile[15], const char* cadena, int posInicial);
void repBitMap(char namePart[10], char outPath[256]);
void repBloques(char namePart[10], char outPath[256]);
void generaDotBloques(char diskPath[256], int posIni, char outPath[256]);
void contentDotBloques(FILE *stream, int posIni, char dir[256]);

/*------------------------------------------------------------------------------------*/
/*-----------------------METODOS Y FUNCIONES PARA MOUNT-------------------------------*/
/*------------------------------------------------------------------------------------*/
int NombreDeParticiones(char ruta[256], char name[300]);
int StartParticiones(char ruta[256], char name[300]);
void mountPart(char path[300], char name[300]);
void Uumount_Metodo(char name[300]);
int getPartSize(char namePart[100]);
int getPartStart(char namePart[100]);


#endif