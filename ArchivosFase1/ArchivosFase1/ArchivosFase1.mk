##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=ArchivosFase1
ConfigurationName      :=Debug
WorkspacePath          :=/home/lezs/Documentos/ArchivosFase1
ProjectPath            :=/home/lezs/Documentos/ArchivosFase1/ArchivosFase1
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Leslie Morales
Date                   :=26/02/17
CodeLitePath           :=/home/lezs/.codelite
LinkerName             :=gcc
SharedObjectLinkerName :=gcc -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="ArchivosFase1.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := gcc
CC       := gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IntermediateDirectory)/montarpart.c$(ObjectSuffix) $(IntermediateDirectory)/validaciones.c$(ObjectSuffix) $(IntermediateDirectory)/Analizador.c$(ObjectSuffix) $(IntermediateDirectory)/GestorDiscos.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.c$(ObjectSuffix): main.c $(IntermediateDirectory)/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/lezs/Documentos/ArchivosFase1/ArchivosFase1/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.c$(ObjectSuffix) -MF$(IntermediateDirectory)/main.c$(DependSuffix) -MM main.c

$(IntermediateDirectory)/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.c$(PreprocessSuffix) main.c

$(IntermediateDirectory)/montarpart.c$(ObjectSuffix): montarpart.c $(IntermediateDirectory)/montarpart.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/lezs/Documentos/ArchivosFase1/ArchivosFase1/montarpart.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/montarpart.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/montarpart.c$(DependSuffix): montarpart.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/montarpart.c$(ObjectSuffix) -MF$(IntermediateDirectory)/montarpart.c$(DependSuffix) -MM montarpart.c

$(IntermediateDirectory)/montarpart.c$(PreprocessSuffix): montarpart.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/montarpart.c$(PreprocessSuffix) montarpart.c

$(IntermediateDirectory)/validaciones.c$(ObjectSuffix): validaciones.c $(IntermediateDirectory)/validaciones.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/lezs/Documentos/ArchivosFase1/ArchivosFase1/validaciones.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/validaciones.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/validaciones.c$(DependSuffix): validaciones.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/validaciones.c$(ObjectSuffix) -MF$(IntermediateDirectory)/validaciones.c$(DependSuffix) -MM validaciones.c

$(IntermediateDirectory)/validaciones.c$(PreprocessSuffix): validaciones.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/validaciones.c$(PreprocessSuffix) validaciones.c

$(IntermediateDirectory)/Analizador.c$(ObjectSuffix): Analizador.c $(IntermediateDirectory)/Analizador.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/lezs/Documentos/ArchivosFase1/ArchivosFase1/Analizador.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Analizador.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Analizador.c$(DependSuffix): Analizador.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Analizador.c$(ObjectSuffix) -MF$(IntermediateDirectory)/Analizador.c$(DependSuffix) -MM Analizador.c

$(IntermediateDirectory)/Analizador.c$(PreprocessSuffix): Analizador.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Analizador.c$(PreprocessSuffix) Analizador.c

$(IntermediateDirectory)/GestorDiscos.c$(ObjectSuffix): GestorDiscos.c $(IntermediateDirectory)/GestorDiscos.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/lezs/Documentos/ArchivosFase1/ArchivosFase1/GestorDiscos.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/GestorDiscos.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/GestorDiscos.c$(DependSuffix): GestorDiscos.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/GestorDiscos.c$(ObjectSuffix) -MF$(IntermediateDirectory)/GestorDiscos.c$(DependSuffix) -MM GestorDiscos.c

$(IntermediateDirectory)/GestorDiscos.c$(PreprocessSuffix): GestorDiscos.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/GestorDiscos.c$(PreprocessSuffix) GestorDiscos.c


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


