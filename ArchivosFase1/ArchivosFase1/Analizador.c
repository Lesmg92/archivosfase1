#include "Analizador.h"
#include "GestorDiscos.h"
/*------------------------------------------------------------------------------------*/
/*---------------------------METODOS Y FUNCIONES AUXILIARES---------------------------*/
/*------------------------------------------------------------------------------------*/

bool startsWith(const char *pre, const char *str){
    int n = strncasecmp(pre, str, strlen(pre)) == 0;
    return n;
}

int buscarEspacios(char *token,char s){
        if (!token || s=='\0')
        return 0;
    for (;*token; token++)
        if (*token == s)
            return 1;
    return 0;
}

char *quitarEspacios(char *tmp,const char *cadena) {

    while((tmp=strchr(cadena,32))!=NULL || (tmp=strchr(cadena,10))!=NULL)
        sprintf(tmp,"%s",tmp+1);
    return tmp;
}

void quitarUltimoEspacio(char *str) {
    int ultimo = strlen(str)-1;
    char aux = str[ultimo];
    if(aux == 32 || aux == '\n'){
        str++[ultimo] = '\0';
        }
   }

void toLowerCase(char s[]) {
   int c = 0;
   while (s[c] != '\0') {
      if (s[c] >= 'A' && s[c] <= 'Z') {
         s[c] = s[c] + 32;
      }
      c++;
   }
}

void analyzeFile(char *path){
    FILE *fptr;
    fptr = fopen(path, "r");
    char linea[254];

    while(fgets(linea, 150, fptr)){
        printf("\nLINEA: --- %s\n", linea);
        orden(linea);
    }

    fclose(fptr); 
}

/*------------------------------------------------------------------------------------*/
/*---------------------------VERIFICAR PARAMETROS CORRECTOS---------------------------*/
/*------------------------------------------------------------------------------------*/

bool nameDskOk(char s[]){
     int c = 0;
   while (s[c] != '\0') {
      if ((s[c] >= 'a' && s[c] <= 'z') || (s[c] >= 'A' && s[c] <= 'Z') || (s[c] >= '0' && s[c] <= '9') || s[c] == '_' || s[c] == '.'|| s[c]==32) {
      } else {
      printf("\t*** Error nombre mal Disco\n");  
      return false;
      }
      c++;
   }
   if(strstr(s, ".dsk") == NULL) {
      return false;
    }
    return true;
}

bool nameOk(char s[]){
     int c = 0;
   while (s[c] != '\0') {
      if ((s[c] >= 'a' && s[c] <= 'z') || (s[c] >= 'A' && s[c] <= 'Z')  || (s[c] >= '0' && s[c] <= '1') || s[c] == '_') {
      } else {
      return false;
      }
      c++;
   }
    return true;
}

bool pathOk(char *str){
    if(strchr(str, 32) != NULL){
      if(strchr(str, 34) != NULL){
          if(str[0] == 34 || str[strlen(str)-1] == 34){
              return true;
            }else{
              return false; //comillas fuera de lugar
            }
      } else {
        return false; // contiene espacios y no comillas
      }
    }
   return true;
}

bool unitOk(char car){
    if(car == 'b' || car == 'k' || car == 'm'){
        return true;
    }
    return false;
}

bool typeOk(char car){
    if(car == 'p' || car == 'e' || car == 'l'){
        return true;
    }
    return false;

}

bool fitOk(const char *str){
       if(strcasecmp(str, "bf") == 0 || strcasecmp(str, "ff") == 0 || strcasecmp(str, "wf") == 0 ) {
      return true;
    }
    return false;
}

bool allocationtOk(const char *str){
       if(strcasecmp(str, "c") == 0 || strcasecmp(str, "e") == 0 || strcasecmp(str, "ix") == 0 ) {
      return true;
    }
    return false;
}

bool deleteOk(const char *str){
       if(strcasecmp(str, "fast") == 0 || strcasecmp(str, "full") == 0 ) {
      return true;
    }
    return false;
}

/*------------------------------------------------------------------------------------*/
/*-------------------------------ACCIONES DE CADA COMANDO-----------------------------*/
/*------------------------------------------------------------------------------------*/

void goToComment(const char *str){
    printf("Comentario: %s \n",str);
}

void goToMkdisk(const char *str){
        int mk_size = 0;
        char *mk_name = malloc(sizeof(char) * 256);
        char *mk_path = malloc(sizeof(char) * 256);
        char mk_unit = 'm';

        const char s[2] = "%&";
        char *token;
        char *end_token;

        bool bmk_size = false;
        bool bmk_path = false;
        bool bmk_name = false;
        token = strtok_r(str, s,&end_token);
        token = strtok_r(NULL,s,&end_token);
             while( token != NULL ){
                      const char aptr[2] = "->";
                      char *end_atributo;
                      char * atributo = strtok_r(token,aptr,&end_atributo);
                      
                        char *aux = malloc(sizeof(char) * 256);
                        strcpy(aux,token);
                      if(startsWith("size",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          mk_size = atoi(atributo);
                          bmk_size = true;
                      }else if(startsWith("path",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarUltimoEspacio(atributo);
                      if(pathOk(atributo)){
                            bmk_path = true;
                            strcpy(mk_path, atributo);
                          }
                      }else if(startsWith("name",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          memset(mk_name, '\0', sizeof(atributo));
                          strcpy(mk_name, atributo);
                          if(nameDskOk(mk_name)){
                          bmk_name = true;
                          }
                      }else if(startsWith("unit",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          mk_unit = atributo[0];
                          if(!unitOk(mk_unit)){
                            // error en unit
                            mk_unit = 'k';
                          }
                      }
                      token = strtok_r(NULL,s,&end_token);
                   }
                   if(bmk_name && bmk_path && bmk_size){
                        printf("\nLos parametros MKDISK son: \nsize:%d \npath:%s \nname:%s \nunit:%c \n", mk_size, mk_path, mk_name, mk_unit);                        
                        if(startsWith("\"",mk_path))
                        {
                            mk_path = strtok(mk_path,"\"");
                        }
                        strcat(mk_path,"/"); 
                        strcat(mk_path,mk_name);
                       crearDisco(mk_path,mk_size,mk_unit);
                   }else{
                        printf("\nPARAMETROS MAL");
                   }
}

void goToRmdisk(const char *str){
    char *rm_path = malloc(sizeof(char) * 256);
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

    while( token != NULL ){
         const char aptr[2] = "->";
         char *end_atributo;
         char *atributo; 
         atributo = strtok_r(token,aptr,&end_atributo);
         
        char *aux = malloc(sizeof(char) * 256);
        strcpy(aux,token);
        if(startsWith("path", aux)){
         atributo = strtok_r(NULL,aptr,&end_atributo);
         quitarUltimoEspacio(atributo);
        if(pathOk(atributo)){
            strcpy(rm_path, atributo);
            rm_path = strtok(rm_path,"\"");
            eliminarDisco(rm_path);
        } else {
          printf("\t*** Error path rmdisk\n");
        }
        }
        token = strtok_r(NULL,s,&end_token);
    }
}

void goToFdisk(const char *str){
    int f_size = 0;
    char f_unit = 'k';
    char *f_path = malloc(sizeof(char) * 256);
    char f_type = 'p';
    char *f_fit = "wf";
    char *f_allocation = "ix";
    char *f_delete = malloc(sizeof(char) * 256); // verificar junto a name y path
    char *f_name = malloc(sizeof(char) * 256);
    int f_add = 0;  // valor unit

    bool bf_size = false;
    bool bf_path = false;
    bool bf_name = false;
    bool bf_useDelete = false;
    bool bf_useAdd = false;

        const char s[2] = "%&";
        char *token;
        char *end_token;
        token = strtok_r(str, s,&end_token);
        token = strtok_r(NULL,s,&end_token);
             while( token != NULL ){
                      const char aptr[2] = "->";
                      char *end_atributo;
                      char * atributo = strtok_r(token,aptr,&end_atributo);
                      
                        char *aux = malloc(sizeof(char) * 256);
                        strcpy(aux,token);
                      if(startsWith("size",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_size = atoi(atributo);
                          bf_size = true;
                      }else if(startsWith("unit",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_unit = atributo[0];
                          if(!unitOk(f_unit)){
                            // error en unit
                            f_unit = 'k';
                          }
                      }else if(startsWith("path",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarUltimoEspacio(atributo);
                          if(pathOk(atributo)){
                            strcpy(f_path, atributo);
                            bf_path = true;
                          }
                      }else if(startsWith("type",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_type = atributo[0];
                          if(!typeOk(f_type)){
                            // error en type
                            f_type = 'p';
                          }
                      }else if(startsWith("fit",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarEspacios(f_fit,atributo);
                          if(!fitOk(f_fit)){
                            // error en fit
                            f_fit = "wf";
                          }
                      }else if(startsWith("allocation",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarEspacios(f_allocation,atributo);
                          if(!allocationtOk(f_allocation)){
                            // error en fit
                            f_allocation = "ix";
                          }
                      }else if(startsWith("delete",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarEspacios(f_delete,atributo);
                          if(!deleteOk(f_delete)){
                            // error en delete
                          }
                          bf_useDelete = true;
                      }else if(startsWith("name",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          quitarEspacios(f_name,atributo);
                          strcpy(f_name,atributo);
                          bf_name = true;
                      }else if(startsWith("add",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          f_add = atoi(atributo);
                          bf_useAdd = true;
                      }
                      token = strtok_r(NULL,s,&end_token);
             }

            //// ------> verificar parametros completos para el uso de fdisk
             if(bf_useDelete){
              if(bf_name && bf_path){
                        printf("\nLos parametros FDISK(DELETE) son: \nname:%s \npath:%s \n", f_name, f_path);
                   }else{
                        printf("\nPARAMETROS MAL FDISK");
                   }

             } else if(bf_useAdd){
              if(bf_name){
                        printf("\nLos parametros FDISK(ADD) son: \nname:%s \nunit:%c \n", f_name, f_unit);
                   }else{
                        printf("\nPARAMETROS MAL FDISK");
                   }
             } else {
              if(bf_name && bf_path && bf_size){
                        printf("\nLos parametros FDISK son: \nsize:%d \npath:%s \nname:%s \nunit:%c \n", f_size, f_path, f_name, f_unit);
                         
                                if(f_unit=='m' || f_unit=='M') 
                                    f_size=f_size*mb;
                                else if(f_unit=='k' || f_unit=='K')
                                    f_size=f_size*kb;
                        if(startsWith("\"",f_path))
                        {
                            f_path = strtok(f_path,"\"");
                        }            
                        crearParticion(f_path, f_name, f_type,f_fit,f_size);
                   }else{
                        printf("\nPARAMETROS MAL FDISK");
                   }
             }
}

void goToMount(const char *str){
        char *m_path = malloc(sizeof(char) * 256);
        char *m_name;

        const char s[2] = "%&";
        char *token;
        char *end_token;

        bool bm_name = false;
        bool bm_path = false;

        token = strtok_r(str, s,&end_token);
        if(token != NULL)
            {
                token = strtok_r(NULL,s,&end_token);
            }
            else
            {
                printListMountedP();
                printf("\n");    
            }
        
             while( token != NULL ){
                      const char aptr[2] = "->";
                      char *end_atributo;
                      char * atributo = strtok_r(token,aptr,&end_atributo);
                      
                        char *aux = malloc(sizeof(char) * 256);
                        strcpy(aux,token);
                      if(startsWith("name",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          m_name = atributo;
                          if(nameOk(atributo)){
                          bm_name = true;
                          }
                      }else if(startsWith("path",aux)){
                          atributo = strtok_r(NULL,aptr,&end_atributo);
                          if(pathOk(atributo)){
                            strcpy(m_path, atributo);
                            bm_path = true;
                          }
                      }
                       token = strtok_r(NULL,s,&end_token);
                   }
                   if(bm_name && bm_path){
                        printf("\nLos parametros MOUNT son: \npath:%s \nname:%s \n", m_path, m_name);
                        mountPart(m_path, m_name);
                   }else{
                        printf("\nPARAMETROS MAL");
                   }
}

void goToUnmount(const char *str){
    
    char *umo_id;
    
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);
    
     while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        
        char *aux = malloc(sizeof(char) * 256);
        strcpy(aux,token);
         if(startsWith("id", aux)){
             atributo = strtok_r(NULL,aptr,&end_atributo);
             quitarUltimoEspacio(atributo);
            if(pathOk(atributo)){
                memset(umo_id, '\0', sizeof(atributo));
                strcpy(umo_id, atributo);
            } else {
              // error en el atributo path
            }
        }
        token = strtok_r(NULL,s,&end_token);
     }
}

void goToDf(const char *str){
    const char s[2] = "%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

        char *aux = malloc(sizeof(char) * 256);
        strcpy(aux,token);

    if(startsWith("k", aux)){
    
    }else if(startsWith("m", aux)){
    
    }else if(startsWith("h", aux)){
        
    }else if(startsWith("i", aux)){
        
    }else{
    // no parametro a consultar    
    }
}

void goToDu(const char *str){
    const char s[2] = "%&";
    char *token;
    char *end_token;
    char *du_path = malloc(sizeof(char) * 256);
    int du_n = 1;
    
    bool bdu_path = false;
    bool bdu_h = false;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

         while( token != NULL ){
             const char aptr[2] = "->";
             char *end_atributo;
             char * atributo = strtok_r(token,aptr,&end_atributo);
             
            char *aux = malloc(sizeof(char) * 256);
            strcpy(aux,token);
            if(startsWith("n", aux)){
                 atributo = strtok_r(NULL,aptr,&end_atributo);
                 du_n = atoi(atributo);
            }else if(startsWith("h", aux)){
                bdu_h = true;
            }else if(startsWith("path", aux)){
                 atributo = strtok_r(NULL,aptr,&end_atributo);
                 quitarUltimoEspacio(atributo);
                if(pathOk(atributo)){
                    strcpy(du_path, atributo);
                    bdu_path = true;
                    } else {
                  // error en el atributo
                    }
            }else{
            // parametro de más  
            }
            token = strtok_r(NULL,s,&end_token);
    }
    if(bdu_h && bdu_path){
       printf("\nLos parametros DU son: \npath:%s", bdu_path);
    }else{
       printf("\nPARAMETROS MAL DISK USED");
    }
}

void goToMkfs(const char *str){
    
    char *mkfs_type;
    char *mkfs_id = malloc(sizeof(char) * 256);
    char *mkfs_fs;
    int mkfs_add;
    char mkfs_unit;
    
    bool bmkfs_id = false;
    bool bmkfs_add = false;
    
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

    
    
    while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        
        char *aux = malloc(sizeof(char) * 256);
        strcpy(aux,token);
        if(startsWith("id", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            strcpy(mkfs_id, atributo);
            if(nameDskOk(mkfs_id)){
               bmkfs_id = true;
            }
        }else if(startsWith("type", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            quitarEspacios(mkfs_type,atributo);
            if(!deleteOk(mkfs_type)){
               // error en delete
            }
        }else if(startsWith("add", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            mkfs_add = atoi(atributo);
            bmkfs_add = true;
        }else if(startsWith("unit", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            mkfs_unit = atributo[0];
            if(!unitOk(mkfs_unit)){
               // error en unit
               mkfs_unit = 'k';
            }
        }else if(startsWith("fs", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            quitarEspacios(mkfs_fs,atributo);
        }
        token = strtok_r(NULL,s,&end_token);
    }               
         if(bmkfs_id)
            {
                if(bmkfs_add)
                    {
                        modificarFormatoPart(mkfs_id, mkfs_add, mkfs_unit);       
                    }
                    else
                    {
                        formatoParticion(mkfs_id, mkfs_type, mkfs_fs);   
                    }
            } 
            else 
            {
                 printf("En mkfs falta id\n");
            }
}

void goToMkfile(const char *str){

    char *mkf_id;
    char *mkf_name;
    char *mkf_contenido;
    
    bool bmkf_id = false;
    bool bmkf_name = false;
    bool bmkf_contenido = false;
    
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

    
    while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        
        char *aux = malloc(sizeof(char) * 256);
        strcpy(aux,token);
        if(startsWith("id", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            memset(mkf_id, '\0', sizeof(atributo));
            strcpy(mkf_id, atributo);
            bmkf_id = true;
        }else if(startsWith("name", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            quitarEspacios(mkf_name,atributo);
            bmkf_name = true;
        }else if(startsWith("contenido", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            memset(mkf_contenido, '\0', sizeof(atributo));
            strcpy(mkf_contenido, atributo);
            bmkf_contenido = true;
        }
        token = strtok_r(NULL,s,&end_token);
    }
    
    if(bmkf_id && bmkf_name && bmkf_contenido)
        {
           guardarArchivo(mkf_id, mkf_name, mkf_contenido); 
        }
        else
        {
            printf("En mkfile faltan parámetros\n");
        }
}

void goToRep(const char *str){    
    
    char *rep_name;
    char *rep_path = malloc(sizeof(char) * 256);
    char *rep_id;
    
    const char s[2] = "&%";
    char *token;
    char *end_token;
    
    bool brep_id = false;
    bool brep_name = false;
    bool brep_path = false;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);

    
    while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        
        char *aux = malloc(sizeof(char) * 256);
        strcpy(aux,token);
        if(startsWith("id", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            memset(rep_id, '\0', sizeof(atributo));
            strcpy(rep_id, atributo);
            brep_id = true;
        }else if(startsWith("name", aux)){
            atributo = strtok_r(NULL,aptr,&end_atributo);
            quitarEspacios(rep_name,atributo);
            toLowerCase(rep_name);
            brep_name = true;
        }else if(startsWith("path", aux)){
             atributo = strtok_r(NULL,aptr,&end_atributo);
             quitarUltimoEspacio(atributo);
            if(pathOk(atributo)){
                strcpy(rep_path, atributo);
                brep_path = true;
            } else {
              // error en el atributo path
            }
     
        }
        token = strtok_r(NULL,s,&end_token);
    }
    
    if(brep_id && brep_name && brep_path)
        {
            if(strcasecmp(rep_name, "mbr")==0)
            {
                
            }
            else if(strcasecmp(rep_name, "disk")==0)
            {
                
            }
            else if(strcasecmp(rep_name, "block")==0)
            {
                    repBloques(rep_id,rep_path); 
            }
            else if(strcasecmp(rep_name, "bm_block")==0)
            {
                    repBitMap(rep_id,rep_path);
            }
        }
        else
            {
            printf("Faltan parametros en Rep\n");
            }

}

void goToExec(const char *str){
    
    char *exe_path = malloc(sizeof(char) * 256);
    
    const char s[2] = "&%";
    char *token;
    char *end_token;

    token = strtok_r(str, s,&end_token);
    token = strtok_r(NULL,s,&end_token);
    
     while( token != NULL ){
        const char aptr[2] = "->";
        char *end_atributo;
        char * atributo = strtok_r(token,aptr,&end_atributo);
        char *aux = malloc(sizeof(char) * 256);
        strcpy(aux,token);
         if(startsWith("path", aux)){
             atributo = strtok_r(NULL,aptr,&end_atributo);
             quitarUltimoEspacio(atributo);
            if(pathOk(atributo)){
                strcpy(exe_path, atributo);
            } else {
              // error en el atributo path
            }
        }
        token = strtok_r(NULL,s,&end_token);
     }
     
     analyzeFile(exe_path); // exec &path->/home/lezs/Escritorio/archivo.txt

}

/*------------------------------------------------------------------------------------*/
/*-------------------------REDIRIGIR ORDEN RESPECTIVA---------------------------------*/
/*------------------------------------------------------------------------------------*/

int orden(const char *str){
    char *aux = malloc(sizeof(char) * 256);
    strcpy(aux, str);
    if(startsWith("#", aux)){
    goToComment(str);
    return 1;
    }else if(startsWith("mkdisk", str)){
    goToMkdisk(str);
    return 2;
    }else if(startsWith("rmdisk",str)){
    goToRmdisk(str);
    return 3;
    }else if(startsWith("fdisk",str)){
    goToFdisk(str);
    return 4;
    }else if(startsWith("mount",str)){
    goToMount(str);
    return 5;
    }else if(startsWith("unmount",str)){
    goToUnmount(str);
    return 6;
    }else if(startsWith("df",str)){
    goToDf(str);
    return 7;
    }else if(startsWith("du",str)){
    goToDu(str);
    return 8;
    }else if(startsWith("mkfs",str)){
    goToMkfs(str);
    return 9;
    }else if(startsWith("mkfile",str)){
    goToMkfile(str);
    return 10;
    }else if(startsWith("rep",str)){
    goToRep(str);
    return 11;
    }else if(startsWith("exec",str)){
    goToExec(str);
    return 12;
    }
return 0;
}


