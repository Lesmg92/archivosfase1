#include "GestorDiscos.h"

    VerFer direc;
    NodoLD mount;

/*------------------------------------------------------------------------------------*/
/*--------------------METODOS Y FUNCIONES PARA GESTION DE DISCOS----------------------*/
/*------------------------------------------------------------------------------------*/

void crearDisco(char dir[256],int sizeDisk,char tipo)
{
    FILE *archivoptr; //puntero del archivo
    archivoptr = fopen(dir,"wb+"); 
    if(archivoptr!=NULL) 
    {
        if(tipo=='m' || tipo=='M') 
            sizeDisk=sizeDisk*mb;
        else if(tipo=='k' || tipo=='K')
            sizeDisk=sizeDisk*kb;
        char *buffer = calloc(sizeDisk,sizeof(char)); //reservar ram
        fseek(archivoptr,0,SEEK_SET); // ptr a cero

        fwrite(buffer,sizeDisk,1,archivoptr); 
        fflush(archivoptr); 
        
        partition part1; // crea particion inicializando valores
        part1.part_status='0';
        part1.part_size=0;
        part1.part_start=0;
        part1.part_type=' ';
        part1.part_fit=' ';
        strcpy(part1.part_name,"");
        
        mbr mbrO;  // crea mbr asignando las particiones vacias
        mbrO.mbr_disk_signature = 1;
        mbrO.mbr_fecha_creacion = time(0);
        mbrO.mbr_partition_1=part1;
        mbrO.mbr_partition_2=part1;
        mbrO.mbr_partition_3=part1;
        mbrO.mbr_partition_4=part1;
        mbrO.mbr_tamano = sizeDisk;
        
        fseek(archivoptr,0,SEEK_SET); //regresa a 0 el ptr
        
        fwrite(&mbrO,sizeof(mbr),1,archivoptr);
        fclose(archivoptr); //escribe buffer y cierra
    }
    else
        printf("Error el archivo no existe\n");
}

void leerDisco(char dir[256])
{
    printf("---------------------\n");
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbrO;
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr
        
        if(mbrO.mbr_partition_1.part_status=='1' )
        {
            printf("%s\n",mbrO.mbr_partition_1.part_name);
        }
        else
            printf("Particion 1 vacia\n");
        if(mbrO.mbr_partition_2.part_status=='1' )
        {
            printf("%s\n",mbrO.mbr_partition_2.part_name);
        }
        else
            printf("Particion 2 vacia\n");
        if(mbrO.mbr_partition_3.part_status=='1' )
        {
            printf("%s\n",mbrO.mbr_partition_3.part_name);
        }
        else
            printf("Particion 3 vacia\n");
        if(mbrO.mbr_partition_4.part_status=='1' )
        {
            printf("%s\n",mbrO.mbr_partition_4.part_name);
        }
        else
            printf("Particion 4 vacia\n");

        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

void eliminarDisco(const char dir[256])
{
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    printf("**¿Desea eliminar el disco: [%s]?\n",dir);
    char conf[2];
    gets(conf);
    if(conf[0] == 's' && archivoptr!=NULL)
    {
        fclose(archivoptr);
        if(remove(dir)==0) 
            printf("El disco ha sido borrado\n");
        else
            printf("El disco ha no sido borrado\n");
    }
    else
        printf("Error el archivo no existe\n");
}

/*------------------------------------------------------------------------------------*/
/*------------------METODOS Y FUNCIONES PARA GESTION DE PARTICIONES-------------------*/
/*------------------------------------------------------------------------------------*/

int getStartPart(const mbr mbr0, int sizeNPart, int pActual){
    
    int ptri = sizeof(mbr0) + 1;
    int ptrf = mbr0.mbr_tamano;
    
    partition parts[4];
    parts[0] = mbr0.mbr_partition_1;
    parts[1] = mbr0.mbr_partition_2;
    parts[2] = mbr0.mbr_partition_3;
    parts[3] = mbr0.mbr_partition_4;
    
    bool partsb[4];
    partsb[0] = mbr0.mbr_partition_1.part_status == '0';
    partsb[1] = mbr0.mbr_partition_2.part_status == '0';
    partsb[2] = mbr0.mbr_partition_3.part_status == '0';
    partsb[3] = mbr0.mbr_partition_4.part_status == '0';
   
    
   int contb = pActual-1;
   
   if(contb != 0){
       int pinicio = parts[contb-1].part_start;
       int pfin = parts[contb-1].part_size;
       ptri = pinicio + pfin +1 ;
       }
   
    while(contb < 3){
        if(partsb[contb]){
        bool cabe = true;
        int conta = contb + 1;
        while(conta<3 && cabe){
            if(!partsb[conta]){
                ptrf = parts[conta].part_start;
                int sizeSpace = ptrf - ptri;
                if(sizeSpace >= sizeNPart){
                    return ptri;
                } else {
                    cabe = false;
                    contb = conta;
                }
            }
            conta++;
        }
        if(cabe){
            if(!partsb[3]){
                ptrf = parts[conta].part_start;
                int sizeSpace = ptrf - ptri;
                if(sizeSpace >= sizeNPart){
                    return ptri;}
                }else {
                int sizeSpace = ptrf - ptri;
                if(sizeSpace >= sizeNPart){
                    return ptri;
                } 
            } 
        } else{
            ptrf = mbr0.mbr_tamano;
            }     
        contb++;
        }else{
            ptri = parts[contb].part_start + parts[contb].part_size + 1;
        }
    }     
   if(contb == 3 && partsb[3]){
            ptri = parts[contb-1].part_start + parts[contb-1].part_size + 1;
            int sizeSpace = ptrf - ptri;
            if(sizeSpace >= sizeNPart){
                    return ptri;
                }
        }
    return -1;
}

void crearParticion(char dir[256],char nom[16],char tipo, char fit, int sizePart)
{
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbrO;
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr

        if(mbrO.mbr_partition_1.part_status == '0')
            {
                int n = getStartPart(mbrO,sizePart,1);
                if(n == -1){
                    printf("Error al crear la particion");
                }else{
                mbrO.mbr_partition_1.part_status='1';
                mbrO.mbr_partition_1.part_type=tipo;
                mbrO.mbr_partition_1.part_fit=fit;
                mbrO.mbr_partition_1.part_start= n;
                mbrO.mbr_partition_1.part_size=sizePart;
                strcpy(mbrO.mbr_partition_1.part_name,nom);   
                }       
            }
        else if(mbrO.mbr_partition_2.part_status == '0')
            {
                int n = getStartPart(mbrO,sizePart,2);
                if(n == -1){
                    printf("Error al crear la particion");
                }else{
                mbrO.mbr_partition_2.part_status='1';
                mbrO.mbr_partition_2.part_type=tipo; 
                mbrO.mbr_partition_2.part_fit=fit;
                mbrO.mbr_partition_2.part_start=n;
                mbrO.mbr_partition_2.part_size=sizePart;
                strcpy(mbrO.mbr_partition_2.part_name,nom);
                }
            }
        else if(mbrO.mbr_partition_3.part_status == '0')
            {
                int n = getStartPart(mbrO,sizePart,3);
                if(n == -1){
                    printf("Error al crear la particion");
                }else{
                mbrO.mbr_partition_3.part_status='1';
                mbrO.mbr_partition_3.part_type=tipo; 
                mbrO.mbr_partition_3.part_fit=fit;
                mbrO.mbr_partition_3.part_start=n;
                mbrO.mbr_partition_3.part_size=sizePart;
                strcpy(mbrO.mbr_partition_3.part_name,nom);
                }
            }
        else if(mbrO.mbr_partition_4.part_status == '0')
            {
                int n = getStartPart(mbrO,sizePart,4);
                if(n == -1){
                    printf("Error al crear la particion");
                }else{
                mbrO.mbr_partition_4.part_status='1';
                mbrO.mbr_partition_4.part_type=tipo; 
                mbrO.mbr_partition_4.part_fit=fit;
                mbrO.mbr_partition_4.part_start=n;
                mbrO.mbr_partition_4.part_size=sizePart;
                strcpy(mbrO.mbr_partition_4.part_name,nom);    
                }        
            }
        else
            {
            printf("Las cuatro particiones estan ocupadas\n");    
            }
            
        fseek(archivoptr,0,SEEK_SET);
        fwrite(&mbrO,sizeof(mbr),1,archivoptr);
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

void eliminarParticion(char dir[256],char nom[16])
{
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbrO;
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fread(&mbrO,sizeof(mbr),1,archivoptr); //lee mbr

        if(strcmp(mbrO.mbr_partition_1.part_name,nom) == 0)
            {
                mbrO.mbr_partition_1.part_status='0';
                mbrO.mbr_partition_1.part_type= '0'; //fit
                mbrO.mbr_partition_1.part_start= 0 ;
                mbrO.mbr_partition_1.part_size= 0;
                strcpy(mbrO.mbr_partition_1.part_name, "");   
            }
        else if(strcmp(mbrO.mbr_partition_2.part_name,nom) == 0)
            {
                mbrO.mbr_partition_2.part_status='0';
                mbrO.mbr_partition_2.part_type= '0'; //fit
                mbrO.mbr_partition_2.part_start= 0 ;
                mbrO.mbr_partition_2.part_size= 0;
                strcpy(mbrO.mbr_partition_2.part_name,"");
            }
        else if(strcmp(mbrO.mbr_partition_3.part_name,nom) == 0)
            {
                mbrO.mbr_partition_3.part_status='0';
                mbrO.mbr_partition_3.part_type='0'; //fit
                mbrO.mbr_partition_3.part_start=0;
                mbrO.mbr_partition_3.part_size=0;
                strcpy(mbrO.mbr_partition_3.part_name,"");
            }
        else if(strcmp(mbrO.mbr_partition_4.part_name,nom) == 0)
            {
                mbrO.mbr_partition_4.part_status='0';
                mbrO.mbr_partition_4.part_type='0'; //fit
                mbrO.mbr_partition_4.part_start=0;
                mbrO.mbr_partition_4.part_size=0;
                strcpy(mbrO.mbr_partition_4.part_name,"");       
            }
        else
            {
            printf("No existe particion con ese nombre\n");    
            }
            
        fseek(archivoptr,0,SEEK_SET);
        fwrite(&mbrO,sizeof(mbr),1,archivoptr);
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

/*------------------------------------------------------------------------------------*/
/*--------------METODOS Y FUNCIONES PARA FORMATO DE PARTICIONES MONTADAS--------------*/
/*------------------------------------------------------------------------------------*/

char *getPartPath(char namePart[100])
    {
         if(NumR(namePart)==-1 || NumR(namePart)==0){
            printf("Error, no esta montada esta particion");
        }else{
         char *ruta=NULL;
            ruta=func(namePart);
            }
    }

void formatoParticion(char namePart[10], char type, char fs){
    
    char *dir = getPartPath(namePart);
    int posIniP = getPartStart(namePart);
    int sizeP = getPartSize(namePart);

    char bloque[50];
        
    int n = (sizeP)/(1+sizeof(bloque));
    int posBitMap = posIniP + sizeof(n)+1;
    
    char bitMap[n];
    int i = 0;
    for(i; i<n;i++){
            bitMap[i]='0';
        }
    
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    if(archivoptr!=NULL)
    {
        fseek(archivoptr, posIniP,SEEK_SET); 
        fwrite(&n,sizeof(n),1,archivoptr);
        fseek(archivoptr, posBitMap,SEEK_SET); 
        fwrite(&bitMap,sizeof(bitMap),1,archivoptr);
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}


void modificarFormatoPart(char namePart[10], int addInt, char unit){
    
    char *dir = getPartPath(namePart);
    int posIniP = getPartStart(namePart);
    int sizeP = getPartSize(namePart);
    
    int sizeSA;
    bool hasContent = false;
    int add;
    char bloque[50];
    
    if(unit=='m' || unit=='M') 
            add=addInt*mb;
        else if(unit=='k' || unit=='K')
            add=addInt*kb;
        else if(unit=='b' || unit=='B')
            add=addInt;
        else
            printf("no es valida la unidad, debe ser m,k,b");
 
        
        FILE *archivoptr;
                archivoptr = fopen(dir,"rb+");
                if(archivoptr!=NULL)
                {
                    fseek(archivoptr, posIniP,SEEK_SET); 
                    fread(&sizeSA,sizeof(sizeSA),1,archivoptr);
                    char bitMapAux[sizeSA];
                    fseek(archivoptr, posIniP+sizeof(sizeSA),SEEK_SET); 
                    fread(&bitMapAux,sizeof(bitMapAux),1,archivoptr);
                    int i = 0;
                    while(i<sizeSA){
                            if(bitMapAux[i] == '1'){
                                hasContent = true;
                            }
                            i++;
                        }
                    sizeSA = sizeSA * 50;
                    fseek(archivoptr,0,SEEK_SET); //ptr a 0
                    fclose(archivoptr);
                }
                else
                    printf("Error el archivo no existe\n");    
    

        int dif = sizeSA + add;
        
        if(hasContent)
            {
                printf("El sistema de archivos ya contiene archivos\n");
            }
        else
            {
            if(dif<55)
                {
                    printf("Error al cambiar tamaño particion resta mas de lo posible\n");
                }
            else if(dif>sizeP)
                {
                    printf("Error al cambiar tamaño particion suma mas de lo posible\n");
                }
            else
                {  
                    int n = (dif)/(1+sizeof(bloque));
                    int posBitMap = posIniP + sizeof(n);                
                    char bitMap[n];
                    int i = 0;
                        for(i; i<n;i++){
                               bitMap[i]='0';
                          }
                    FILE *archivoptr;
                    archivoptr = fopen(dir,"rb+");
                    if(archivoptr!=NULL)
                    {
                        fseek(archivoptr, posIniP,SEEK_SET); 
                        fwrite(&dif,sizeof(dif),1,archivoptr);
                        fseek(archivoptr, posBitMap,SEEK_SET); 
                        fwrite(&bitMap,sizeof(bitMap),1,archivoptr);
                        fseek(archivoptr,0,SEEK_SET); //ptr a 0
                        fclose(archivoptr);
                    }
                    else
                        printf("Error el archivo no existe\n");    
            } 
        }
}

/*------------------------------------------------------------------------------------*/
/*--------------METODOS Y FUNCIONES PARA GUARDAR EN PARTICION FORMATEADA--------------*/
/*------------------------------------------------------------------------------------*/

void guardarArchivo(char namePart[10], char nameFile[15], const char* cadena){
    
    char *dir = getPartPath(namePart);
    int posIniP = getPartStart(namePart);
    int sizeP = getPartSize(namePart);
    char fitP = 'b'; //getPartFit(namePart); 
     
     switch(fitP){
         case 'b':
            {
                bestFit(dir, nameFile, cadena, posIniP);
                break;
            }
         case 'f':
            {
                firstFit(dir, nameFile, cadena, posIniP);
                break;
            }
         case 'w':
            {
                worstFit(dir, nameFile, cadena, posIniP);
                break;
            }
        default:
            {
            
            }
    }
}

 
/*###################################### PRIMER AJUSTE #######################################*/
void firstFit(char dir[256], const char nameFile[15], const char* cadena, int posInicial){
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    int n;
    int sizeFile = strlen(cadena);
    
    // encuentro el numero de bloques necesarios para ingresar el archivo
    int bNeeded = 0;
    int divMod = sizeFile % 35;
        if(divMod == 0)
            {
                bNeeded = sizeFile / 35;
            }
        else
            {
                bNeeded = (sizeFile / 35)+1;
            }
    
    if(archivoptr!=NULL)
    {
        // leo el valor de n (posiciones del bitmap)
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial,SEEK_SET); 
        fread(&n,sizeof(n),1,archivoptr); //lee n
        
        // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
        char bitMap[n];
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n) + 1,SEEK_SET); 
        fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
        
        // recorro el vector para encontrar el primer espacio disponible con suficientes bloques
        int i = 0;
        int bEmpty = 0;  
        int lastUtilB = 0;
        
        while(i<n)
            {
                if(bitMap[i]=='0')
                    {
                        bEmpty++;
                        if(bEmpty == bNeeded)
                            {
                                lastUtilB = i; // obtengo la posicion del ultimo bloque de la serie de bloques donde cabe
                                i = n;
                            }
                    }
                i++;
            }
            
        // obtengo la primera posicion de la serie de bloques de la serie    
        int firstUtilB = lastUtilB - (bNeeded-1);
        i = firstUtilB;
        // lleno la serie de bloques en el bitMap con 1
        int cont = 0;
        for(cont; cont < bNeeded; cont++)
            {
                bitMap[i] = '1';
                i++;
            }
            
        // coloco el apuntador en la posicion inicial de la serie de bloques en particion 
        // y escribo el archivo 
        //int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + firstUtilB *50 + 1;
        char nameFileC[15];
        strcpy(nameFileC,nameFile);
        i = firstUtilB;
        cont = 0;
        int contB = 0;
        
        for(cont; cont < bNeeded; cont++)
            {
                char content[35];
                if(i == bNeeded - 1)
                    {
                        strcpy (content,"                                  ");
                        strcpy (content,cadena+contB);
                    }
                else
                    {
                        strncpy (content,cadena+contB,35);
                    }
                
                int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + i*50 + 1;
                fseek(archivoptr,0,SEEK_SET);
                fseek(archivoptr,posIniB,SEEK_SET);
                fwrite(&nameFileC, sizeof(nameFileC), 1, archivoptr);
                fseek(archivoptr,posIniB + sizeof(nameFileC),SEEK_SET);
                fwrite(&content, sizeof(content), 1, archivoptr);
                contB = contB + 35;
                i++;
            }  
        
        // reescribo el bitMap en el archivo
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1,SEEK_SET); 
        fwrite(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
        fclose(archivoptr);        
    }
    else
        printf("Error el archivo no existe\n");
}


/*###################################### MEJOR AJUSTE #####################################*/
void bestFit(char dir[256], char nameFile[15], char* cadena, int posInicial){
    FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    int n;
    int sizeFile = strlen(cadena);
    
    // encuentro el numero de bloques necesarios para ingresar el archivo
    int bNeeded = 0;
    int divMod = sizeFile % 35;
        if(divMod == 0)
            {
                bNeeded = sizeFile / 35;
            }
        else
            {
                bNeeded = (sizeFile / 35)+1;
            }
    
    
    if(archivoptr!=NULL)
    {
        // leo el valor de n (posiciones del bitmap)
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial,SEEK_SET); 
        fread(&n,sizeof(n),1,archivoptr); //lee n
        // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
        char bitMap[n];
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1, SEEK_SET); 
        fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap

        int serieUtilB[n][2];
        // recorro el vector para encontrar los espacios en donde quepa
        int block = 0;
        int pos = 0;
        int bEmpty = 0;  
        int lastUtilB = 0;
        int nVacios = 0;
        // lleno la matriz con los valores de: [inicioSerieUtil][numeroDeBloques]
        while(block < n)
            {
                if(bitMap[block] == '0')
                        nVacios++;
                else
                    {
                        serieUtilB[pos][0] = block - nVacios;
                        serieUtilB[pos][1] = nVacios;
                        pos++;
                        nVacios = 0;
                    }
                block++;
            }
        if(nVacios == n){
            serieUtilB[0][0]= 0;
            serieUtilB[0][1]= n;
            }
        // recorro la matriz para encontrar el menor diferente de 0
        int i = 0;
        int posMin = 0;
        int min = serieUtilB[0][1];
        
        for(i; i<n;i++){
                    int valNow = serieUtilB[i][1];
                    if(valNow<min && valNow !=0 && bNeeded)
                        {
                            min = valNow;
                            posMin = i;
                        }
            }
        
        // obtengo la primera posicion de la serie de bloques de la serie    
        int firstUtilB = posMin;
        i = firstUtilB;
        // lleno la serie de bloques en el bitMap con 1
        int cont = 0;
        for(cont; cont < bNeeded; cont++)
            {
                bitMap[i] = '1';
                i++;
            }
            
        // coloco el apuntador en la posicion inicial de la serie de bloques en particion 
        // y escribo el archivo 
        //int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + firstUtilB *50 + 1;
        char nameFileC[15];
        strcpy(nameFileC,nameFile);
        i = firstUtilB;
        cont = 0;
        int contB = 0;
        
        for(cont; cont < bNeeded; cont++)
            {
                char content[35];
                if(i == bNeeded - 1)
                    {
                        strcpy (content,"                                  ");
                        strcpy (content,cadena+contB);
                    }
                else
                    {
                        strncpy (content,cadena+contB,35);
                    }
                
                int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + i*50 + 1;
                fseek(archivoptr,0,SEEK_SET);
                fseek(archivoptr,posIniB,SEEK_SET);
                fwrite(&nameFileC, sizeof(nameFileC), 1, archivoptr);
                fseek(archivoptr,posIniB + sizeof(nameFileC),SEEK_SET);
                fwrite(&content, sizeof(content), 1, archivoptr);
                contB = contB + 35;
                i++;
            }  
        
        // reescribo el bitMap en el archivo
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1,SEEK_SET); 
        fwrite(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
        fclose(archivoptr);           
    }
    else
        printf("Error el archivo no existe\n");
    
}

/*###################################### PEOR AJUSTE ######################################*/
void worstFit(char dir[256], char nameFile[15], const char* cadena, int posInicial){
     FILE *archivoptr;
    archivoptr = fopen(dir,"rb+");
    int n;
    int sizeFile = strlen(cadena);
    
    // encuentro el numero de bloques necesarios para ingresar el archivo
    int bNeeded = 0;
    int divMod = sizeFile % 35;
        if(divMod == 0)
            {
                bNeeded = sizeFile / 35;
            }
        else
            {
                bNeeded = (sizeFile / 35)+1;
            }
    
    
    if(archivoptr!=NULL)
    {
        // leo el valor de n (posiciones del bitmap)
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial,SEEK_SET); 
        fread(&n,sizeof(n),1,archivoptr); //lee n
        // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
        char bitMap[n];
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1, SEEK_SET); 
        fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap

        int serieUtilB[n][2];
        // recorro el vector para encontrar los espacios en donde quepa
        int block = 0;
        int pos = 0;
        int bEmpty = 0;  
        int lastUtilB = 0;
        int nVacios = 0;
        // lleno la matriz con los valores de: [inicioSerieUtil][numeroDeBloques]
        while(block < n)
            {
                if(bitMap[block] == '0')
                        nVacios++;
                else
                    {
                        serieUtilB[pos][0] = block - nVacios;
                        serieUtilB[pos][1] = nVacios;
                        pos++;
                        nVacios = 0;
                    }
                block++;
            }
        if(nVacios == n){
            serieUtilB[0][0]= 0;
            serieUtilB[0][1]= n;
            }
        // recorro la matriz para encontrar el menor diferente de 0
        int i = 0;
        int posMin = 0;
        int max = serieUtilB[0][1];
        
        for(i; i<n;i++){
                    int valNow = serieUtilB[i][1];
                    if(valNow>max && valNow !=0 && bNeeded)
                        {
                            max = valNow;
                            posMin = i;
                        }
            }
        
        // obtengo la primera posicion de la serie de bloques de la serie    
        int firstUtilB = posMin;
        i = firstUtilB;
        // lleno la serie de bloques en el bitMap con 1
        int cont = 0;
        for(cont; cont < bNeeded; cont++)
            {
                bitMap[i] = '1';
                i++;
            }
            
        // coloco el apuntador en la posicion inicial de la serie de bloques en particion 
        // y escribo el archivo 
        //int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + firstUtilB *50 + 1;
        char nameFileC[15];
        strcpy(nameFileC,nameFile);
        i = firstUtilB;
        cont = 0;
        int contB = 0;
        
        for(cont; cont < bNeeded; cont++)
            {
                char content[35];
                if(i == bNeeded - 1)
                    {
                        strcpy (content,"                                  ");
                        strcpy (content,cadena+contB);
                    }
                else
                    {
                        strncpy (content,cadena+contB,35);
                    }
                
                int posIniB = posInicial + sizeof(n) + sizeof(bitMap) + i*50 + 1;
                fseek(archivoptr,0,SEEK_SET);
                fseek(archivoptr,posIniB,SEEK_SET);
                fwrite(&nameFileC, sizeof(nameFileC), 1, archivoptr);
                fseek(archivoptr,posIniB + sizeof(nameFileC),SEEK_SET);
                fwrite(&content, sizeof(content), 1, archivoptr);
                contB = contB + 35;
                i++;
            }  
        
        // reescribo el bitMap en el archivo
        fseek(archivoptr,0,SEEK_SET); //ptr a 0
        fseek(archivoptr,posInicial + sizeof(n)+1,SEEK_SET); 
        fwrite(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
        fclose(archivoptr);           
    }
    else
        printf("Error el archivo no existe\n");
}

/*------------------------------------------------------------------------------------*/
/*--------------------METODOS Y FUNCIONES PARA REPORTAR ARCHIVOS----------------------*/
/*------------------------------------------------------------------------------------*/

void repBitMap(char namePart[10], char outPath[256])
    {
        char *dir = getPartPath(namePart);
        int posIniP = getPartStart(namePart);
        
        FILE *archivoptr;
        archivoptr = fopen(dir,"rb+");
        int n;
        
        FILE *fp;
        fp = fopen(outPath,"w"); //use outpath   // Open File in Write Mode
                
        if(archivoptr!=NULL)
            {
                fseek(archivoptr,0,SEEK_SET); //ptr a 0
                fseek(archivoptr,posIniP,SEEK_SET); 
                fread(&n,sizeof(n),1,archivoptr); //lee n
                // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
                char bitMap[n];
                fseek(archivoptr,posIniP + sizeof(n) +1,SEEK_SET); 
                fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
                
                int posIniB = posIniP+sizeof(n)+sizeof(bitMap)+1;
                int modLineas = n%20;
                int lineas = n/20;
                if(modLineas!=0)
                    lineas++;
                int i = 0;
                int cLinea =0;
                for(i; i<lineas;i++)
                    {
                        char tLinea[20];
                        strcpy(tLinea, bitMap+cLinea);
                        fprintf(fp, "%s\n",tLinea);
                        cLinea+20;
                    }
               fclose(archivoptr);
               fclose(fp); 
               char comandoOpen[256];
               char comandoStart[15] = "gnome-open ";
               strcpy(comandoOpen,comandoStart);
               strcat(comandoOpen,outPath);
               system(comandoOpen);
            }
        else
            printf("Error el archivo no existe\n");
    }

void repBloques(char namePart[10], char outPath[256])
    {
        char *dir = getPartPath(namePart);
        int posIniP = getPartStart(namePart);
    
        generaDotBloques(dir, posIniP,outPath);
    }


void generaDotBloques(char diskPath[256], int posIni, char outPath[256]) 
    {
        FILE *fp;
        char outPatDot[256];
        strcpy(outPatDot,outPath);
        strcat(outPatDot,".dot");
        fp = fopen(outPatDot,"w");   // Open File in Write Mode
        
        
        char outPatPng[256];
        strcpy(outPatPng,outPath);
        strcat(outPatPng,".png");  
        
        fprintf(fp,"digraph G { \n\trankdir = LR;\n\tnode [shape=record];\n\n\t");
        contentDotBloques(fp, posIni, diskPath );
        fprintf(fp,"}");
        fclose(fp);
        
        char comandoDot[100]= "dot ";
        
        strcat(comandoDot, outPatDot);
        strcat(comandoDot, " -o ");
        strcat(comandoDot, outPatPng);
        strcat(comandoDot, " -Tpng ");
        
        system(comandoDot); 

        char comandoOpen[256]= "gnome-open ";
        strcat(comandoOpen,outPath);
        strcat(comandoOpen, ".png");
        
        system(comandoOpen);
    }
    
void contentDotBloques(FILE *stream, int posIni, char dir[256]){
        FILE *archivoptr;
        archivoptr = fopen(dir,"rb+");
        int n;
        
        if(archivoptr!=NULL)
        {
            fseek(archivoptr,0,SEEK_SET); //ptr a 0
            fseek(archivoptr,posIni,SEEK_SET); 
            fread(&n,sizeof(n),1,archivoptr); //lee n
            // leo el bitmap desde el archivo y lo guardo en vector bitMap[]    
            char bitMap[n];
            fseek(archivoptr,posIni + sizeof(n) +1,SEEK_SET); 
            fread(&bitMap,sizeof(bitMap),1,archivoptr); //lee bitMap
            
            int posIniB = posIni+sizeof(n)+sizeof(bitMap)+1;
            int i = 0;
            char prevName[15] = "";
            for(i; i<n;i++)
                {   
                    if(bitMap[i] == '1')
                    {                            
                        char nameFile[15];
                        fseek(archivoptr,0,SEEK_SET); //ptr a 0
                        fseek(archivoptr,posIniB + (i*50),SEEK_SET); 
                        fread(&nameFile,sizeof(nameFile),1,archivoptr); //lee n
                        
                        char contentFile[35];
                        fseek(archivoptr,0,SEEK_SET); //ptr a 0
                        fseek(archivoptr,posIniB + (i*50) + sizeof(nameFile),SEEK_SET); 
                        fread(&contentFile,sizeof(contentFile),1,archivoptr); //lee n
                        
                        fprintf(stream, "bloque%d [label=\"Bloque %d|%s|%s\"];\n\t", i,i,nameFile,contentFile);
                        if(strcmp(prevName, nameFile) == 0)
                            {
                                fprintf(stream, "bloque%d -> bloque%d;\n\t", i-1,i);
                            }
                        strcpy(prevName,nameFile);
                    }
                }
                fclose(archivoptr);  
        }
        else
            printf("Error el archivo no existe\n");
    }
    

/*------------------------------------------------------------------------------------*/
/*-----------------------METODOS Y FUNCIONES PARA MOUNT-------------------------------*/
/*------------------------------------------------------------------------------------*/

//Verifica Si existe un nombre
int NombreDeParticiones(char ruta[256], char name[300])
{
    int u=0,con=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbr1;
        fseek(archivoptr,0,SEEK_SET);
        fread(&mbr1,sizeof(mbr),1,archivoptr);
        if(mbr1.mbr_partition_1.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_1.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_1.part_size;
            }
        }
        if(mbr1.mbr_partition_2.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_2.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_2.part_size;
            }
        }
        if(mbr1.mbr_partition_3.part_status=='1' )
        {
           u = strcasecmp(mbr1.mbr_partition_3.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_3.part_size;
            }
        }

        if(mbr1.mbr_partition_4.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_4.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_4.part_size;
            }
        }

        return con;
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

int StartParticiones(char ruta[256], char name[300])
{
    int u=0,con=0;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    if(archivoptr!=NULL)
    {
        mbr mbr1;
        fseek(archivoptr,0,SEEK_SET);
        fread(&mbr1,sizeof(mbr),1,archivoptr);
        if(mbr1.mbr_partition_1.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_1.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_1.part_start;
            }
        }
        if(mbr1.mbr_partition_2.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_2.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_2.part_start;
            }
        }
        if(mbr1.mbr_partition_3.part_status=='1' )
        {
           u = strcasecmp(mbr1.mbr_partition_3.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_3.part_start;
            }
        }

        if(mbr1.mbr_partition_4.part_status=='1' )
        {
          u = strcasecmp(mbr1.mbr_partition_4.part_name,name);
          if(u==0)
            {
            con=mbr1.mbr_partition_4.part_start;
            }
        }

        return con;
        fclose(archivoptr);
    }
    else
        printf("Error el archivo no existe\n");
}

 //METODO PARA MONTAR
void mountPart(char path[300], char name[300]){
    strcpy(mount.path,path);
    char letras[27]="abcdefghijklmnopqrstuvwyxz";
    char valor[300]="";
    char *temp;
    char *rest =path;
    while((temp = strtok_r(rest, "/", &rest)))
    {
           printf("Esta imprimiendo en name %s\n",temp);
           strcpy(valor,temp);
    }
     int x=0;
     x=buscarD12(valor);
     printf("En x: %d\n",x);
     if(x==-1){
         direc.name=letras[0];
        strcpy(direc.path,valor);
         VerificarDiscoPart(direc);
     }else if(x==0){
         int u=0;
         u=tamanoLista();
         direc.name=letras[u];
        strcpy(direc.path,valor);
         VerificarDiscoPart(direc);
     }
     
             int y=0;
        y=buscarD(valor);
         printf("En y: %d\n",y);
        if(y==-1 || y==0){
            strcpy(mount.name,name);
            strcpy(mount.nameDisk,valor);
            char ui[50]="",montado[50]="";
            sprintf(ui, "%d",NumR(valor));
            printf("En el ui=%s\n",ui);
            montado[0]='v';
            montado[1]='d';
            montado[2]=ParaMontarE(valor);
            strcat(montado,ui);
       strcpy(mount.id,montado);
            insertarAdelanteLD(mount);

        }
        
           strcpy(path,mount.path);
}

void Uumount_Metodo(char name[300]){
    char valor[300]="";
    char *temp;
    char *rest =name;
    while((temp = strtok_r(rest, ",", &rest)))
    {
           printf("Esta imprimiendo en name %s\n",temp);

           strcpy(valor,temp);

    }
    if(ParaElim(valor)==0 ||ParaElim(valor)==-1 ){
    printf("Error el id esta mal, no hay ninguna particion montada con ese id %s\n");
    }else{
       eliminarD(ParaElim(valor)); 
       //imprimirNodoDVerificarDiscoPart();
                     //imprimirLD();
    }
    
        }
        
int getPartSize(char namePart[100]){
        if(NumR(namePart)==-1 || NumR(namePart)==0){
            printf("Error, no esta montada esta particion");
        }else{
            printf("Entro al else \n");
            char *ruta=NULL, *name=NULL;
            ruta=func(namePart);
            printf("ruta: %s \n",ruta);
            name=Nunc(namePart);
            return NombreDeParticiones(ruta,name);
            }
             
     }
     
int getPartStart(char namePart[100]){
        if(NumR(namePart)==-1 || NumR(namePart)==0){
            printf("Error, no esta montada esta particion");
        }else{
            printf("Entro al else \n");
            char *ruta=NULL, *name=NULL;
            ruta=func(namePart);
            printf("ruta: %s \n",ruta);
            name=Nunc(namePart);
            return StartParticiones(ruta,name);
            }
        }
        
  